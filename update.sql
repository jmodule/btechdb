-- these are the changes to the mercroster database that support multi-tenant use
alter table crew add column members_id int(11) default NULL;
alter table equipment add column members_id int(11) default NULL;
alter table unit add column members_id int(11) default NULL;
alter table members add column email varchar(64) default NULL;

alter table equipment modify column crew int(11) default NULL;
alter table equipment modify column troid int(11) default NULL;

alter table equipmenttypes add column config varchar(45) default NULL;
alter table equipmenttypes add column card_code tinyint(1) default 0;

alter table technicalreadouts add column battle_value int(11) default 0;
alter table technicalreadouts add column point_value int(11) default 0;
alter table technicalreadouts add column cost int(11) default 0;
alter table technicalreadouts add column year_introduced int(11) default 0;
alter table technicalreadouts add column rules_level tinyint(1) default 1;
alter table technicalreadouts add column technology tinyint(1) default 0;
alter table technicalreadouts add column unit_role_id int(11) default NULL; -- FK to unit_role

alter table technicalreadouts add column as_size tinyint(1) default NULL;
alter table technicalreadouts add column as_move varchar(20) default NULL;
alter table technicalreadouts add column as_armor tinyint(1) default 0;
alter table technicalreadouts add column as_structure tinyint(1) default 0;
alter table technicalreadouts add column as_damage_threshold tinyint(1) default NULL;
alter table technicalreadouts add column as_short float default NULL;
alter table technicalreadouts add column as_medium float default NULL;
alter table technicalreadouts add column as_long float default NULL;
alter table technicalreadouts add column as_overheat float default NULL;
alter table technicalreadouts add column as_special varchar(100) default NULL;

CREATE TABLE `unit_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO unit_role (name, notes) VALUES ('Ambusher', 'Slow, lightly armored, short ranged weapons');
INSERT INTO unit_role (name, notes) VALUES ('Brawler', 'Line units; fair speed, armor, and range');
INSERT INTO unit_role (name, notes) VALUES ('Juggernaut', 'Slow and tough');
INSERT INTO unit_role (name, notes) VALUES ('Missile Boat', 'Long range, indirect fire');
INSERT INTO unit_role (name, notes) VALUES ('Scout', 'Fast, light armor and weapons');
INSERT INTO unit_role (name, notes) VALUES ('Skirmisher', 'Fast, heavy armor');
INSERT INTO unit_role (name, notes) VALUES ('Sniper', 'Long range, stealth or speed');
INSERT INTO unit_role (name, notes) VALUES ('Striker', 'Fast, light armor, short ranged weapons');

ALTER TABLE ranks ADD COLUMN short_name VARCHAR(10) DEFAULT NULL;

UPDATE ranks SET short_name = 'RCT' WHERE number = 1;
UPDATE ranks SET short_name = 'PVT' WHERE number = 2;
UPDATE ranks SET short_name = 'CPL' WHERE number = 3;
UPDATE ranks SET short_name = 'SGT' WHERE number = 4;
UPDATE ranks SET short_name = 'MSG' WHERE number = 5;
UPDATE ranks SET short_name = 'WO' WHERE number = 6;
UPDATE ranks SET short_name = 'LT' WHERE number = 7;
UPDATE ranks SET short_name = 'CPT' WHERE number = 8;
UPDATE ranks SET short_name = 'MAJ' WHERE number = 9;
UPDATE ranks SET short_name = 'COL' WHERE number = 10;
UPDATE ranks SET short_name = 'LTG' WHERE number = 11;
UPDATE ranks SET short_name = 'MG' WHERE number = 12;
UPDATE ranks SET short_name = 'GEN' WHERE number = 13;

ALTER TABLE technicalreadouts ADD COLUMN mul_id INT DEFAULT NULL;

ALTER TABLE crew ADD COLUMN gender TINYINT(1) DEFAULT 0;