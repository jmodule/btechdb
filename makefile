.PHONY: install default

default: install

install:
	rsync -avz --exclude="*.pyc" --exclude="__pycache__/" --exclude="btechroster/static/images/unitlevel" --exclude="btechroster/static/images/unittype" btechroster littlesoldiers.net:btechdb/

push:
	# this is only needed at work where I have multiple keys and accounts in play
	ssh-agent sh -c 'ssh-add ~/.ssh/id_bitbucket; git push origin master'
