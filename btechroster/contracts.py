from flask import (
    Blueprint, render_template, request, flash, redirect, url_for)
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from sqlobject import SQLObjectNotFound
from wtforms import DateField, StringField, SubmitField, TextAreaField, SelectField
from wtforms.validators import InputRequired, Optional

from btechroster import model

bp = Blueprint('contracts', __name__, url_prefix='/contracts')


@bp.route('/')
@login_required
def index():
    s = model.Contracts.select(orderBy=model.Contracts.q.start)
    return render_template('contracts/index.html', contract_list=s)


class ContractForm(FlaskForm):
    name = StringField('Name', validators=[InputRequired()])
    start = DateField('Start', validators=[InputRequired()])
    end = DateField('End', validators=[InputRequired()])
    employer = StringField('Employer', validators=[Optional()])
    mission_type = StringField('Mission Type', validators=[Optional()])
    target = StringField('Target', validators=[Optional()])
    result = StringField('Result', validators=[Optional()])
    save_and_close = SubmitField('Save')


@bp.route('/add', methods=('GET', 'POST'))
@login_required
def add():
    if current_user.is_admin():
        form = ContractForm()
        if request.method == 'POST':
            if form.validate_on_submit():
                model.Contracts(
                    name=form.name.data,
                    start=form.start.data,
                    end=form.end.data,
                    employer=form.employer.data,
                    missionType=form.mission_type.data,
                    target=form.target.data,
                    result=form.result.data,
                )
                flash('Contract added', 'success')
                return redirect(url_for('contracts.index'))
            else:
                flash('Please correct the errors', 'danger')
        return render_template('contracts/edit.html', form=form)
    else:
        flash('You do not have permission to add a contract', 'danger')
        return redirect(url_for('contracts.index'))


@bp.route('/<int:contract_id>/edit', methods=('GET', 'POST'))
@login_required
def edit(contract_id):
    if current_user.is_admin():
        contract_rec = model.Contracts.get(contract_id)
        form = ContractForm()
        if request.method == 'POST':
            if form.validate_on_submit():
                contract_rec.name = form.name.data
                contract_rec.start = form.start.data
                contract_rec.end = form.end.data
                contract_rec.employer = form.employer.data
                contract_rec.missionType = form.mission_type.data
                contract_rec.target = form.target.data
                contract_rec.result = form.result.data
                flash('Contract updated', 'success')
                return redirect(url_for('contracts.index'))
            else:
                flash('Please correct the errors', 'danger')
        else:
            form.name.data = contract_rec.name
            form.start.data = contract_rec.start
            form.end.data = contract_rec.end
            form.employer.data = contract_rec.employer
            form.mission_type.data = contract_rec.missionType
            form.target.data = contract_rec.target
            form.result.data = contract_rec.result
        return render_template('contracts/edit.html', form=form)
    else:
        flash('You do not have permission to edit a contract', 'danger')
        return redirect(url_for('contracts.index'))


@bp.route('/<int:contract_id>/show', methods=('GET',))
@login_required
def show(contract_id):
    contract_rec = model.Contracts.get(contract_id)
    return render_template('contracts/show.html', rec=contract_rec, current_user_level=current_user.memberType)


@bp.route('/logs/<int:log_id>/show', methods=('GET',))
@login_required
def show_log(log_id):
    log_entry = model.Logentry.get(log_id)
    return render_template('contracts/show_log.html', rec=log_entry)


class LogForm(FlaskForm):
    log_type = SelectField('Log Type', coerce=int)
    during = SelectField('Contract', coerce=int)
    start_date = DateField('Start', validators=[InputRequired()])
    end_date = DateField('End', validators=[Optional()])
    place = StringField('Place', validators=[Optional()])
    topic = StringField('Title', validators=[Optional()])
    text = TextAreaField('Text', validators=[Optional()])
    save_and_close = SubmitField('Save')


@bp.route('/logs/create', methods=('GET', 'POST'))
@login_required
def create_log():
    form = LogForm()

    log_types_choices = model.log_types_allowed(current_user.memberType, reading=False)
    if len(log_types_choices) > 0:
        form.log_type.choices = log_types_choices

        contract_choices = [(ct.id, ct.name) for ct in model.Contracts.select()]
        contract_choices.insert(0, (-1, ''))
        form.during.choices = contract_choices

        if request.method == 'POST':
            if form.validate_on_submit():
                log_type = model.Logtypes.get(form.log_type.data)
                rec = model.Logentry(
                    start=form.start_date.data,
                    end=form.end_date.data,
                    place=form.place.data,
                    text=form.text.data,
                    topic=form.topic.data,
                    contract=form.during.data,
                    logtype=log_type,
                )
                flash('Log entry added')
                if form.during.data > -1:
                    return redirect(url_for('contracts.show', contract_id=form.during.data))
                else:
                    return redirect(url_for('contracts.index'))
            else:
                flash('Please fix the errors', 'danger')
        else:
            contract_calendar = model.Dates.select().getOne()
            try:
                form.during.data = int(request.args.get('contract_id', '-1'))
            except ValueError:
                pass
            form.start_date.data = contract_calendar.currentdate
        return render_template('contracts/edit_log.html', form=form)
    else:
        flash('You do not have permission to add a log entry.', 'danger')
        return redirect(url_for('contracts.show', contract_id=request.args.get('contract_id', '')))


@bp.route('/logs/<int:log_id>/edit', methods=('GET', 'POST'))
@login_required
def edit_log(log_id):
    form = LogForm()

    log_types_choices = model.log_types_allowed(current_user.memberType, reading=False)
    if len(log_types_choices) > 0:
        form.log_type.choices = log_types_choices

        contract_choices = [(ct.id, ct.name) for ct in model.Contracts.select()]
        contract_choices.insert(0, (-1, ''))
        form.during.choices = contract_choices

        rec = model.Logentry.get(log_id)

        if request.method == 'POST':
            if form.validate_on_submit():
                try:
                    this_contract = model.Contracts.get(form.during.data)
                except SQLObjectNotFound:
                    this_contract = None

                rec.contract = this_contract
                rec.logtype = model.Logtypes.get(form.log_type.data)
                rec.start = form.start_date.data
                rec.end = form.end_date.data
                rec.place = form.place.data
                rec.topic = form.topic.data
                rec.text = form.text.data

                flash('Saved the log entry', 'success')
                return redirect(url_for('contracts.show_log', log_id=rec.id))
            else:
                flash('Please fix the errors', 'danger')
        else:
            form.during.data = rec.contract.id
            form.log_type.data = rec.logtype.id
            form.start_date.data = rec.start
            form.end_date.data = rec.end
            form.place.data = rec.place
            form.topic.data = rec.topic
            form.text.data = rec.text
        return render_template('contracts/edit_log.html', form=form)
    else:
        flash('You do not have permission to edit a log entry.', 'danger')
        return redirect(url_for('contracts.show_log', log_id=log_id))
