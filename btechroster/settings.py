import glob

import sqlobject
from flask import (Blueprint, flash, redirect, render_template, url_for, request, current_app)
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from wtforms import (SelectField, SubmitField, DateField, StringField, IntegerField, BooleanField)
from wtforms.validators import InputRequired, Optional
from wtforms.widgets import ColorInput

from pathlib import Path

from btechroster import model

bp = Blueprint('settings', __name__, url_prefix='/settings')


@bp.route('/')
@login_required
def index():
    try:
        equipment_types = model.EquipmentTypes.select(orderBy=model.EquipmentTypes.q.prefpos)
        skill_types = model.SkillTypes.select(orderBy=model.SkillTypes.q.name)
        personnel_types = model.CrewTypes.select(orderBy=model.CrewTypes.q.prefpos)
        unit_types = model.UnitTypes.select(orderBy=model.UnitTypes.q.id)
        unit_levels = model.UnitLevel.select(orderBy=model.UnitLevel.q.prefpos)
        ranks = model.Ranks.select(orderBy=model.Ranks.q.id)
        special = model.AbilityTypes.select(orderBy=model.AbilityTypes.q.name)
        result = render_template('settings/index.html',
                                 equipment_types=equipment_types,
                                 skill_types=skill_types,
                                 personnel_types=personnel_types,
                                 unit_types=unit_types,
                                 unit_levels=unit_levels,
                                 ranks=ranks,
                                 special=special)
    except Exception as exc:
        result = "{}".format(exc)
    return result


years = [(i, i) for i in list(range(2600, 3300))]
months = [(i, i) for i in list(range(1, 12))]
days = [(i, i) for i in list(range(1, 31))]


class DatesForm(FlaskForm):
    starting_date = DateField('Starting Date')
    current_date = DateField('Current Date')
    ending_date = DateField('Ending Date')
    save = SubmitField('Save')


@bp.route('/dates/edit', methods=('GET', 'POST'))
@login_required
def dates():
    record = model.Dates.get(1)
    form = DatesForm()

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.startingdate = form.starting_date.data
                record.currentdate = form.current_date.data
                record.endingdate = form.ending_date.data
                flash('Campaign dates updated', 'success')
            else:
                flash('Please correct the errors', 'danger')
        else:
            flash('Only administrators can change settings', 'warning')
    else:
        form.starting_date.data = record.startingdate
        form.current_date.data = record.currentdate
        form.ending_date.data = record.endingdate
    return render_template('settings/dates.html', form=form)


class EquipmentTypesForm(FlaskForm):
    type_name = StringField('Name', validators=[InputRequired()])
    id_number = IntegerField('ID Number', validators=[InputRequired()])
    max_weight = IntegerField('Max Weight', validators=[Optional()])
    min_weight = IntegerField('Min Weight', validators=[Optional()])
    weight_step = IntegerField('Weight Step', validators=[Optional()])
    weight_scale = SelectField('Weight Scale', coerce=int, validators=[Optional()])
    requirement = SelectField('Requirement', coerce=int, validators=[Optional()])
    in_use = BooleanField('Used')
    save = SubmitField('Apply')
    save_and_close = SubmitField('Save')


@bp.route('/eqpttypes/<int:eqpt_type_id>/edit', methods=('GET', 'POST'))
@login_required
def equipment_types(eqpt_type_id):
    record = model.EquipmentTypes.get(eqpt_type_id)

    form = EquipmentTypesForm()

    form.weight_scale.choices = [(i[0], i[1]) for i in enumerate(model.eqpt_type_weight_scale)]
    form.requirement.choices = [(c.id, c.type) for c in model.CrewTypes.select(orderBy='id')]

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.name = form.type_name.data
                record.license = form.id_number.data
                record.maxWeight = form.max_weight.data
                record.minWeight = form.min_weight.data
                record.weightStep = form.weight_step.data
                record.weightScale = model.eqpt_type_weight_scale[form.weight_scale.data]
                record.crewType = model.CrewTypes.get(form.requirement.data)
                record.used = form.in_use.data
                flash('Updated the equipment type', 'success')
                if form.save_and_close.data:
                    return redirect(url_for('settings.index'))
            else:
                flash('Please correct the errors', 'danger')
        else:
            flash('Only administrators can change settings', 'warning')
    else:
        form.type_name.data = record.name
        form.id_number.data = record.license
        form.max_weight.data = record.maxWeight
        form.min_weight.data = record.minWeight
        form.weight_step.data = record.weightStep
        form.requirement.data = record.crewType.id
        form.in_use.data = record.used
    return render_template('settings/equipment_types.html', form=form, eqpt_type_id=eqpt_type_id)


class SkillTypesForm(FlaskForm):
    skill_name = StringField('Name', validators=[InputRequired()])
    short_name = StringField('Short Name', validators=[InputRequired()])
    save = SubmitField('Save')


@bp.route('/skilltypes/<int:skill_type_id>/edit', methods=('GET', 'POST'))
@login_required
def skill_types(skill_type_id):
    record = model.SkillTypes.get(skill_type_id)
    form = SkillTypesForm()

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.name = form.skill_name.data
                record.shortname = form.short_name.data
                flash('Updated the skill type', 'success')
                return redirect(url_for('settings.index'))
            else:
                flash('Please correct the errors', 'danger')
        else:
            flash('Only administrators can change settings', 'warning')
    else:
        form.skill_name.data = record.name
        form.short_name.data = record.shortname
    return render_template('settings/skills.html', form=form, skill_type_id=skill_type_id)


class AbilityTypesForm(FlaskForm):
    ability_name = StringField('Name', validators=[InputRequired()])
    save_and_close = SubmitField('Save')


@bp.route('/ablitytypes/<int:ability_type_id>/edit', methods=('GET', 'POST'))
@login_required
def ability_types(ability_type_id):
    record = model.AbilityTypes.get(ability_type_id)
    form = AbilityTypesForm()

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.name = form.ability_name.data
                flash('Updated the ability type', 'success')
                return redirect(url_for('settings.index'))
            else:
                flash('Please correct the errors', 'danger')
        else:
            flash('Only administrators can change settings', 'warning')
    else:
        form.ability_name.data = record.name

    return render_template('settings/special_abilities.html', form=form, ability_type_id=ability_type_id)


class RanksForm(FlaskForm):
    rank_name = StringField('Name', validators=[InputRequired()])
    short_name = StringField('Short Name', validators=[InputRequired()])
    save_and_close = SubmitField('Save')


@bp.route('/ranks/<int:rank_id>/edit', methods=('GET', 'POST'))
@login_required
def ranks(rank_id):
    record = model.Ranks.get(rank_id)
    form = RanksForm()

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.name = form.rank_name.data
                record.shortName = form.short_name.data
                flash('Updated the rank', 'success')
                return redirect(url_for('settings.index'))
            else:
                flash('Please correct the errors', 'danger')
        else:
            flash('Only administrators can change settings', 'warning')
    else:
        form.rank_name.data = record.name
        form.short_name.data = record.shortName

    return render_template('settings/personnel_ranks.html', form=form, rank_id=rank_id)


class UnitLevelsForm(FlaskForm):
    level_name = StringField('Name', validators=[InputRequired()])
    pref_pos = IntegerField('Position', validators=[InputRequired()])
    picture = SelectField('Requirement', validators=[Optional()])
    save_and_close = SubmitField('Save')


@bp.route('/unitlevels/<int:unit_level_id>/edit', methods=('GET', 'POST'))
@login_required
def unit_levels(unit_level_id):
    record = model.UnitLevel.get(unit_level_id)
    form = UnitLevelsForm()

    unit_pictures = Path(current_app.static_folder).joinpath('images/unitlevel')

    unit_level_pictures = unit_pictures.glob("*.png")

    form.picture.choices = [(p.name, p.name) for i, p in enumerate(sorted(unit_level_pictures))]

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.name = form.level_name.data
                record.prefpos = form.pref_pos.data
                record.picture = form.picture.data
                return redirect(url_for('settings.index'))
            else:
                flash("Please correct the errors", 'danger')
        else:
            flash("Only administrators can change settings.", 'warning')
    else:
        form.level_name.data = record.name
        form.pref_pos.data = record.prefpos
        form.picture.data = record.picture

    return render_template('settings/unit_levels.html', form=form, unit_level_id=unit_level_id)


class UnitTypesForm(FlaskForm):
    type_name = StringField('Name', validators=[InputRequired()])
    color = StringField('Color', widget=ColorInput(), validators=[Optional()])
    save_and_close = SubmitField('Save')


@bp.route('/unittypes/<int:unit_type_id>/edit', methods=('GET', 'POST'))
@login_required
def unit_types(unit_type_id):
    record = model.UnitTypes.get(unit_type_id)
    form = UnitTypesForm()

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.name = form.type_name.data
                record.color = form.color.data
                return redirect(url_for('settings.index'))
            else:
                flash("Please correct the errors", 'danger')
        else:
            flash("Only administrators can change settings.", 'warning')
    else:
        form.type_name.data = record.name
        form.color.data = record.color

    return render_template('settings/unit_types.html', form=form, unit_type_id=unit_type_id)


class CrewTypesForm(FlaskForm):
    type_name = StringField('Name', validators=[InputRequired()])
    equipment = SelectField('Equipment', coerce=int, validators=[Optional()])
    is_squad = BooleanField('Squad Leader')
    can_equip = BooleanField('Can Use Equipment')
    save = SubmitField('Apply')
    save_and_close = SubmitField('Save')

# <!-- name
# equipment (select)
# squad leader (checkbox)
# equippable (checkbox)
# change button
# delete button
#
# Requirements section
# add requirement/skill (select)
#
# skill (remove)
#
# site position (up/down) -->


@bp.route('/crewtype/<int:crew_type_id>/edit', methods=('GET', 'POST'))
@login_required
def crew_types(crew_type_id):
    record = model.CrewTypes.get(crew_type_id)
    form = CrewTypesForm()

    eqpt_types = [(e.id, e.name) for e in model.EquipmentTypes.select()]
    eqpt_types.insert(0, (-1, 'No equipment selected...'))

    form.equipment.choices = eqpt_types

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                record.type = form.type_name.data
                if form.equipment.data != -1:
                    try:
                        record.equipmentType = model.EquipmentTypes.get(form.equipment.data)
                    except sqlobject.SQLObjectNotFound:
                        pass
                else:
                    if record.equipmentType is not None:
                        record.equipmentType = None
                record.squad = form.is_squad.data
                record.equipment = form.can_equip.data
                return redirect(url_for('settings.index'))
            else:
                flash("Please correct the errors", 'danger')
        else:
            flash("Only administrators can change settings.", 'warning')
    else:
        form.type_name.data = record.type
        try:
            if record.equipmentType is not None:
                form.equipment.data = record.equipmentType.id
        except sqlobject.SQLObjectNotFound:
            pass
        form.is_squad.data = record.squad
        form.can_equip.data = record.equipment

    return render_template('settings/personnel_types.html', form=form, crew_type_id=crew_type_id)
