import csv
import os
from pathlib import Path

from flask import (Blueprint, flash, redirect, render_template, url_for, request, current_app, jsonify, abort)
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileField, FileAllowed
from sqlobject import SQLObjectNotFound, AND
from werkzeug.utils import secure_filename
from wtforms import SubmitField, StringField, SelectField, DecimalField, TextAreaField, BooleanField, IntegerField
from wtforms.validators import InputRequired, NumberRange, Optional

from btechroster import model
from btechroster.utils import MtfFile, BlkFile
from btechroster.utils.mtf_file import MtfImportError

bp = Blueprint('techreadout', __name__, url_prefix='/tro')

#
# Using PV calculations from: https://bg.battletech.com/forums/index.php?PHPSESSID=218koor2vqntshod7lnk6ajkjk&topic=53612.0
#


@bp.app_template_filter()
def number_comma_format(value):
    return format(int(value), ',d')


@bp.route('/')
def index():
    result = model.TechnicalReadouts.select(orderBy=model.TechnicalReadouts.q.name)
    return render_template('techreadout/index.html', tro_list=result)


class EditTroForm(FlaskForm):
    name = StringField('Name', validators=[InputRequired()])
    subtype = StringField('Model', validators=[Optional()])
    tro_type = SelectField('Equipment Type', choices=[], coerce=int)
    weight = IntegerField('Tons', validators=[NumberRange(1, 100, "Weight must be between %(min)s and %(max)s")])
    text = TextAreaField('Text', validators=[InputRequired()])
    battle_value = IntegerField('Battle Value', validators=[Optional()])
    point_value = IntegerField('Point Value', validators=[Optional()])
    cost = DecimalField('Cost', places=2, validators=[Optional()])
    year_introduced = IntegerField('Year Introduced')
    rules_level = SelectField('Rules Level', choices=[(r[0], r[1]) for r in enumerate(model.rules_level)], coerce=int)
    technology = SelectField('Technology', choices=[(t[0], t[1]) for t in enumerate(model.technology)], coerce=int)
    unit_role = SelectField('Unit Role', choices=[], coerce=int)
    as_size = IntegerField('Size', validators=[Optional()])
    as_move = StringField('Move', validators=[Optional()])
    as_armor = IntegerField('Armor', validators=[Optional()])
    as_structure = IntegerField('Structure', validators=[Optional()])
    as_damage_threshold = IntegerField('Damage Threshold (Aerospace)', validators=[Optional()])
    as_short = DecimalField('Short Range Damage', validators=[Optional()])
    as_medium = DecimalField('Medium Range Damage', validators=[Optional()])
    as_long = DecimalField('Long Range Damage', validators=[Optional()])
    as_overheat = DecimalField('Overheat Damage', validators=[Optional()])
    as_special = StringField('Special', validators=[Optional()])
    mul_id = IntegerField('MUL ID #', validators=[Optional()])
    submit = SubmitField('Save')


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    form = EditTroForm()

    form.tro_type.choices = [(r.id, r.name) for r in model.EquipmentTypes.select(orderBy=model.EquipmentTypes.q.prefpos)]
    unit_roles = [(r.id, r.name) for r in model.UnitRole.select()]
    unit_roles.insert(0, (-1, 'Select a role...'))
    form.unit_role.choices = unit_roles

    if current_user.is_admin():
        if request.method == 'POST':
            if form.validate_on_submit():
                if form.unit_role.data > -1:
                    this_unit_role = model.UnitRole.get(form.unit_role.data)
                else:
                    this_unit_role = None
                model.TechnicalReadouts(
                    name=form.name.data,
                    subtype=form.subtype.data,
                    troType=model.EquipmentTypes.get(form.tro_type.data),
                    weight=form.weight.data,
                    text=form.text.data,
                    battleValue=form.battle_value.data,
                    pointValue=form.point_value.data,
                    cost=form.cost.data,
                    yearIntroduced=form.year_introduced.data,
                    rulesLevel=form.rules_level.data,
                    technology=form.technology.data,
                    unitRole=this_unit_role,
                    asSize=form.as_size.data,
                    asMove=form.as_move.data,
                    asArmor=form.as_armor.data,
                    asStructure=form.as_structure.data,
                    asDamageThreshold=form.as_damage_threshold.data,
                    asShort=form.as_short.data,
                    asMedium=form.as_medium.data,
                    asLong=form.as_long.data,
                    asOverheat=form.as_overheat.data,
                    asSpecial=form.as_special.data,
                    mulId=form.mul_id.data,
                )
                flash("Added Tech Readout", 'success')
                return redirect(url_for('techreadout.index'))

        return render_template('techreadout/edit.html', form=form, tro_id=None)
    else:
        flash('Only administrators can create Tech Readouts', 'warning')
        return redirect(url_for('techreadout.index'))


@bp.route('/<int:tro_id>/edit', methods=('GET', 'POST'))
@login_required
def edit(tro_id):
    form = EditTroForm()
    tro = model.TechnicalReadouts.get(tro_id)

    from_show = request.args.get('from_show', None)

    form.tro_type.choices = [(r.id, r.name) for r in model.EquipmentTypes.select(orderBy=model.EquipmentTypes.q.prefpos)]
    unit_roles = [(r.id, r.name) for r in model.UnitRole.select()]
    unit_roles.insert(0, (-1, 'Select a role...'))
    form.unit_role.choices = unit_roles

    if current_user.is_admin():
        if request.method == 'POST':
            if form.validate_on_submit():
                tro.name = form.name.data
                tro.subtype = form.subtype.data
                tro.troType = model.EquipmentTypes.get(form.tro_type.data)
                tro.weight = form.weight.data
                tro.text = form.text.data
                tro.battleValue = form.battle_value.data
                tro.pointValue = form.point_value.data
                tro.cost = form.cost.data
                tro.yearIntroduced = form.year_introduced.data
                tro.rulesLevel = form.rules_level.data
                tro.technology = form.technology.data
                if form.unit_role.data > -1:
                    tro.unitRole = model.UnitRole.get(form.unit_role.data)
                else:
                    tro.unitRole = None
                tro.asSize = form.as_size.data
                tro.asMove = form.as_move.data
                tro.asArmor = form.as_armor.data
                tro.asStructure = form.as_structure.data
                tro.asDamageThreshold = form.as_damage_threshold.data
                tro.asShort = form.as_short.data
                tro.asMedium = form.as_medium.data
                tro.asLong = form.as_long.data
                tro.asOverheat = form.as_overheat.data
                tro.asSpecial = form.as_special.data
                tro.mulId = form.mul_id.data

                flash('Updated {}'.format(form.name.data), 'success')
                return redirect(url_for('techreadout.index'))
            else:
                flash('Please correct the errors', 'warning')
        else:
            form.name.data = tro.name
            form.subtype.data = tro.subtype
            form.tro_type.data = tro.troType.id
            form.weight.data = tro.weight
            form.text.data = tro.text
            form.battle_value.data = tro.battleValue
            form.point_value.data = tro.pointValue
            form.cost.data = tro.cost
            form.year_introduced.data = tro.yearIntroduced
            form.rules_level.data = tro.rulesLevel
            form.technology.data = tro.technology
            if tro.unitRole is not None:
                form.unit_role.data = tro.unitRole.id
            form.as_size.data = tro.asSize
            form.as_move.data = tro.asMove
            form.as_armor.data = tro.asArmor
            form.as_structure.data = tro.asStructure
            form.as_damage_threshold.data = tro.asDamageThreshold
            form.as_short.data = tro.asShort
            form.as_medium.data = tro.asMedium
            form.as_long.data = tro.asLong
            form.as_overheat.data = tro.asOverheat
            form.as_special.data = tro.asSpecial
            form.mul_id.data = tro.mulId

        return render_template('techreadout/edit.html', form=form, tro_id=tro_id, from_show=from_show)
    else:
        flash('Only administrators can edit Tech Readouts', 'warning')
        return redirect(url_for('techreadout.index'))


class ImportMtfForm(FlaskForm):
    mtf_file = FileField('Import MTF File', validators=[
        FileRequired(),
        FileAllowed(['mtf', 'blk'], "MTF and BLK files only!")
    ])
    can_overwrite = BooleanField('Overwrite Existing')
    import_stats = BooleanField('Import Alpha Strike Stats', default='checked')
    submit = SubmitField()


@bp.route('/import', methods=('GET', 'POST'))
@login_required
def import_mtf():
    form = ImportMtfForm()

    if current_user.is_admin():
        if request.method == 'POST':
            if form.validate_on_submit():
                try:
                    f = form.mtf_file.data
                    filename = secure_filename(f.filename)
                    print("FILENAME = ", filename)
                    import_dir = Path(current_app.instance_path).joinpath('imports')
                    if not import_dir.exists():
                        import_dir.mkdir()
                    full_path = import_dir.joinpath(filename)
                    f.save(full_path)
                    if f.filename.endswith(".mtf"):
                        impt = MtfFile(full_path)
                        # FIXME: need a better way to determine the EquipmentTypes value
                        eqpt_type = model.EquipmentTypes.selectBy(config='Biped').getOne()

                        result = model.TechnicalReadouts.select(AND(model.TechnicalReadouts.q.name == impt.name,
                                                                    model.TechnicalReadouts.q.subtype == impt.model))
                        if len(list(result)) == 0:
                            tro = model.TechnicalReadouts(
                                name=impt.name,
                                subtype=impt.model,
                                troType=eqpt_type,
                                weight=impt.get_weight(),
                                battleValue=impt.bv,
                                yearIntroduced=impt.get_year(),
                                technology=impt.get_tech_base_value(),
                                rulesLevel=impt.get_rules_level(),
                                text=impt.tro_text
                            )
                            flash('Imported data from {}'.format(filename), 'success')
                        elif form.can_overwrite.data:
                            tro = result[0]
                            tro.name = impt.name
                            tro.subtype = impt.model
                            tro.troType = eqpt_type
                            tro.weight = impt.get_weight()
                            tro.battleValue = impt.bv
                            tro.yearIntroduced = impt.get_year()
                            tro.technology = impt.get_tech_base_value()
                            tro.rulesLevel = impt.get_rules_level()
                            tro.text = impt.tro_text
                            flash('Updated the record from {}'.format(filename), 'success')
                        else:
                            tro = None
                            flash('The technical readout already exists for {}'.format(impt.get_full_name()), 'warning')
                    else:
                        impt = BlkFile(full_path)
                        eqpt_type = model.EquipmentTypes.select(model.EquipmentTypes.q.name == impt.get_eqpt_type()).getOne()
                        result = model.TechnicalReadouts.select(AND(model.TechnicalReadouts.q.name == impt.name,
                                                                    model.TechnicalReadouts.q.subtype == impt.model))
                        if len(list(result)) == 0:
                            tro = model.TechnicalReadouts(
                                name=impt.name,
                                subtype=impt.model,
                                troType=eqpt_type,
                                weight=impt.tonnage,
                                battleValue=0,
                                yearIntroduced=impt.year,
                                # technology=impt.tech_level,
                                # rulesLevel=impt.source,
                                text=str(impt)
                            )
                            flash('Imported data from {}'.format(filename), 'success')
                        elif form.can_overwrite.data:
                            tro = result[0]
                            tro.name = impt.name
                            tro.subtype = impt.model
                            tro.troType = eqpt_type
                            tro.weight = impt.tonnage
                            tro.battleValue = 0
                            tro.yearIntroduced = impt.year
                            # tro.technology = impt.tech_level
                            # tro.rulesLevel = impt.source
                            tro.text = str(impt)
                            flash('Updated the record from {}'.format(filename), 'success')
                        else:
                            tro = None
                            flash('The technical readout already exists for {}'.format(impt.get_full_name()), 'warning')
                    if tro is not None and form.import_stats.data:
                        stats = search_csv(impt.name, impt.model)
                        if len(stats['entries']) > 0:
                            tro.asMove = stats['entries'][0]['move']
                            tro.asSpecial = stats['entries'][0]['special']
                            try:
                                tro.asSize = int(stats['entries'][0]['size'])
                            except ValueError:
                                pass
                            try:
                                tro.asArmor = int(stats['entries'][0]['arm'])
                            except ValueError:
                                pass
                            try:
                                tro.asDamageThreshold = int(stats['entries'][0]['dt'])
                            except ValueError:
                                pass
                            try:
                                tro.asStructure = int(stats['entries'][0]['str'])
                            except ValueError:
                                pass
                            try:
                                tro.asShort = float(stats['entries'][0]['s'])
                            except ValueError:
                                pass
                            try:
                                tro.asMedium = float(stats['entries'][0]['m'])
                            except ValueError:
                                pass
                            try:
                                tro.asLong = float(stats['entries'][0]['l'])
                            except ValueError:
                                pass
                            try:
                                tro.asOverheat = float(stats['entries'][0]['ov'])
                            except ValueError:
                                pass
                            try:
                                tro.pointValue = int(stats['entries'][0]['pv'])
                            except ValueError:
                                pass
                            flash('Alpha Strike stats imported', 'success')
                        else:
                            flash('Alpha Strike stats not found for this unit', 'info')
                    # Remove the file now that we are done
                    try:
                        full_path.unlink()
                    except Exception as exc:
                        current_app.logger.warning('Failed to remove the file: {}'.format(exc))
                    return redirect(url_for('techreadout.index'))
                except Exception as exc:
                    current_app.logger.warning('Import Failure: {}'.format(exc))
                    flash('Failed to import file, see log for errors.', 'warning')
                    # flash('Exception: {}'.format(exc), 'danger')
        return render_template('techreadout/import_mtf.html', form=form)
    else:
        flash('Only administrators can import MTF files.', 'warning')
        return redirect(url_for('techreadout.index'))


def search_csv(name, model_name):
    result = list()
    errors = []
    if name.strip() != "":
        line_cnt = 0
        try:
            with open(os.path.join(current_app.instance_path, 'alpha_strike_pv.csv'), newline='', encoding='utf-8') as fd:
                reader = csv.reader(fd, delimiter="\t")
                for row in reader:
                    try:
                        line_cnt += 1
                        if name.lower() in row[0].lower():
                            if model_name is None or model_name.lower() in row[1].lower():
                                result.append({
                                    'name': row[0],
                                    'model': row[1],
                                    'type': row[2],
                                    'size': row[3],
                                    'move': row[4],
                                    'arm': row[5],
                                    'dt': row[6],
                                    'str': row[7],
                                    's': row[8],
                                    'm': row[9],
                                    'l': row[10],
                                    'ov': row[11],
                                    'pv': row[12],
                                    'special': row[13],
                                })
                    except UnicodeDecodeError as exc:
                        errors.append('Error on line {}: {}'.format(line_cnt, exc))
        except Exception as exc:
            errors.append('Failed to find name {} because of: {} (line {})'.format(name, exc, line_cnt))
    return {'entries': result, 'errors': errors}


@bp.route('/search', methods=('GET', 'POST'))
def import_csv():
    name = request.args.get('name', '')
    model = request.args.get('model', None)
    return jsonify(search_csv(name, model))


@bp.route('/<int:tro_id>/delete')
@login_required
def delete(tro_id):
    if current_user.is_admin():
        try:
            model.TechnicalReadouts.delete(tro_id)
            flash("Deleted Technical Readout", 'success')
        except SQLObjectNotFound:
            abort(404)
    else:
        flash('Only administrators can delete a Technical Readout', 'warning')
    return redirect(url_for('techreadout.index'))


@bp.route('/<int:tro_id>/show')
def show(tro_id):
    try:
        tro = model.TechnicalReadouts.get(tro_id)
        return render_template('techreadout/show.html', tro=tro)
    except SQLObjectNotFound:
        abort(404)


@bp.route('/json', methods=('GET', 'POST'))
def get_json():
    tro_id = request.args.get('tro_id', '')
    json_out = {}
    try:
        rec = model.TechnicalReadouts.get(int(tro_id))
        json_out['name'] = rec.name
        json_out['subtype'] = rec.subtype
        json_out['weight'] = rec.weight
    except (SQLObjectNotFound, ValueError):
        pass
    return jsonify(json_out)
