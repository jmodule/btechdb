from flask import (
    Blueprint, render_template
)
from sqlobject import IN, AND, DESC

from btechroster import model

bp = Blueprint('landing', __name__)


@bp.route('/')
def index():
    game_dates = model.Dates.get(1)
    contract_result = model.Contracts.select(AND(model.Contracts.q.start <= game_dates.currentdate,
                                                 model.Contracts.q.end >= game_dates.currentdate),
                                             orderBy=DESC(model.Contracts.q.start))
    if len(list(contract_result)) > 0:
        current_location = contract_result[0].target
        current_employer = contract_result[0].employer
    else:
        current_location = 'Hiring Halls'
        current_employer = 'None'
    s = model.Unit.select(IN(model.Unit.q.parent, (0, 1)),
                          orderBy=(model.Unit.q.parent, model.Unit.q.prefpos, model.Unit.q.id))
    return render_template('landing/index.html',
                           units=s,
                           game_dates=game_dates,
                           current_location=current_location,
                           current_employer=current_employer)


@bp.route('/help')
def app_help():
    return render_template('landing/help.html')
