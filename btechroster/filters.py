from flask import Blueprint

bp = Blueprint('filters', __name__)


@bp.app_template_filter('formatdate')
def format_date(value, date_style='medium'):
    if value is None:
        return ''
    if date_style == 'full':
        fmt_str = "%A, %B %d, %Y"
    elif date_style == 'medium':
        fmt_str = '%d %b %Y'
    else:
        fmt_str = '%d/%m/%Y'
    return value.strftime(fmt_str)
