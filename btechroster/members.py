from flask import (
    Blueprint, render_template, flash, request, redirect, url_for
)
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from wtforms import (SelectField, EmailField, PasswordField, SubmitField, DateField, StringField, IntegerField, BooleanField)
from wtforms.validators import InputRequired, Optional, EqualTo

from btechroster import model

bp = Blueprint('members', __name__, url_prefix='/members')


@bp.route('/')
@login_required
def index():
    s = model.Members.select(orderBy=(model.Members.q.lname, model.Members.q.fname))
    return render_template('members/index.html', members=s)


class UserForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired()])
    change_password = BooleanField('Change Password', validators=[Optional()])
    password = PasswordField('Password', validators=[Optional(), EqualTo('password_confirm', message='Password must match')])
    password_confirm = PasswordField('Repeat Password', validators=[Optional()])
    first_name = StringField('First Name', validators=[Optional()])
    last_name = StringField('Last Name', validators=[Optional()])
    member_type = SelectField('Member Type', coerce=int, validators=[InputRequired()])
    email = EmailField('Email', validators=[Optional()])
    save = SubmitField('Save')


@bp.route('/<int:user_id>/edit', methods=('GET', 'POST'))
@login_required
def edit_user(user_id):
    record = model.Members.get(user_id)
    form = UserForm()

    form.member_type.choices = [(k, n) for k, n in model.MEMBER_TYPE.items()]

    if request.method == 'POST':
        if current_user.is_admin():
            if form.validate_on_submit():
                if form.change_password.data:
                    record.set_password(form.password.data)
                record.fname = form.first_name.data
                record.lname = form.last_name.data
                record.memberType = form.member_type.data
                record.email = form.email.data
                flash('Updated the membership information', 'success')
                return redirect(url_for('members.index'))
            else:
                flash('Please correct the errors', 'danger')
        else:
            flash('Only administrators can change settings', 'warning')
    else:
        form.username.data = record.username
        form.first_name.data = record.fname
        form.last_name.data = record.lname
        form.member_type.data = record.memberType
        form.email.data = record.email

    return render_template('members/edit.html', form=form)
