import traceback

import sqlobject
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,
    jsonify, current_app)
from werkzeug.exceptions import abort

from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from wtforms import DateField, StringField, SelectField, TextAreaField, BooleanField, FieldList, SubmitField
from wtforms.validators import InputRequired, Optional

from btechroster import model
from btechroster.utils import generate_random_name, ANCESTRY

bp = Blueprint('roster', __name__, url_prefix='/roster')


@bp.route('/')
@login_required
def index():
    if current_user.is_admin():
        s = model.Crew.select(orderBy=(model.Crew.q.lname, model.Crew.q.fname))
    else:
        s = model.Crew.select(model.Crew.q.members == current_user,
                              orderBy=(model.Crew.q.lname, model.Crew.q.fname))
    return render_template('roster/index.html', crew_list=s)


class CrewForm(FlaskForm):
    rank = SelectField('Rank', coerce=int)
    last_name = StringField('Last Name', validators=[InputRequired()])
    first_name = StringField('First Name', validators=[InputRequired()])
    gender = SelectField('Gender', choices=((0, "Select a gender..."), (1, 'Male'), (2, 'Female')), coerce=int, validators=[InputRequired()])
    birthdate = DateField('Birthdate', validators=[Optional()])
    ancestry = SelectField('Ancestry', choices=[(a[0], a[1]) for a in enumerate(ANCESTRY)], coerce=int, validators=[Optional()])
    callsign = StringField('Callsign')
    position = SelectField('Position', coerce=int, validators=[Optional()])
    joindate = DateField('Join Date', validators=[Optional()])
    ability = SelectField('Special Ability', coerce=int, validators=[Optional()])
    ability_note = StringField('Special Ability Notes', validators=[Optional()])
    notes = TextAreaField('Notes')
    save = SubmitField('Apply')
    save_and_close = SubmitField('Save')


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if current_user.is_player():
        ranks = model.Ranks.select(model.Ranks.q.name != '-', orderBy=model.Ranks.q.id)
        positions = model.CrewTypes.select(orderBy=model.CrewTypes.q.prefpos)
        abilities = model.AbilityTypes.select(orderBy=model.AbilityTypes.q.name)
        form = CrewForm()
        form.rank.choices = [(r.id, r.name) for r in ranks]
        form.position.choices = [(p.id, p.type) for p in positions]
        form.ability.choices = [(a.id, a.name) for a in abilities]
        if form.validate_on_submit():
            join_date = model.get_current_date()
            birth_date = model.get_random_birth_date()
            rank = model.Ranks.get(form.rank.data)
            player = model.Members.get(current_user.id)
            new_crew = model.Crew(rank=rank,
                                  lname=form.last_name.data,
                                  fname=form.first_name.data,
                                  callsign=form.callsign.data,
                                  joiningdate=join_date,
                                  notes=form.notes.data,
                                  bday=birth_date,
                                  gender=form.gender.data,
                                  members=player)
            flash('Crew member added.', 'success')
            flash('Edit the crew member to add positions and skills.', 'info')
            return redirect(url_for('roster.update', crew_id=new_crew.id))

        return render_template('roster/create.html', form=form)
    else:
        flash('You do not have permission to create crew', 'danger')
        return redirect(url_for('roster.index'))


def get_crew(crew_id, check_owner=True):
    crew_rec = model.Crew.get(crew_id)

    if crew_rec is None:
        abort(404, "Crew record {0} doesn't exist".format(crew_id))

    if check_owner and crew_rec.members.id != current_user.id and not current_user.is_admin():
        abort(403)

    return crew_rec


@bp.route('/<int:crew_id>/update', methods=('GET', 'POST'))
@login_required
def update(crew_id):
    if current_user.is_player():
        try:
            crew_rec = get_crew(crew_id)

            ranks = model.Ranks.select(model.Ranks.q.name != '-', orderBy=model.Ranks.q.id)

            positions = [(p.id, p.type) for p in model.CrewTypes.select(orderBy=model.CrewTypes.q.prefpos)]
            positions.insert(0, (-1, 'Add a position...'))

            abilities = [(a.id, a.name) for a in model.AbilityTypes.select(orderBy=model.AbilityTypes.q.name)]
            abilities.insert(0, (-1, 'Add a special ability...'))

            form = CrewForm()
            form.rank.choices = [(r.id, r.name) for r in ranks]
            form.position.choices = positions
            form.ability.choices = abilities

            if request.method == 'POST':
                if form.validate_on_submit():
                    crew_rec.lname = form.last_name.data
                    crew_rec.fname = form.first_name.data
                    crew_rec.bday = form.birthdate.data
                    crew_rec.callsign = form.callsign.data
                    crew_rec.gender = form.gender.data
                    crew_rec.joiningdate = form.joindate.data
                    crew_rec.rank = model.Ranks.get(form.rank.data)
                    # Add a new position if it is not a duplicate
                    if form.position.data > -1:
                        new_pos = model.CrewTypes.get(form.position.data)
                        can_add = True
                        if len(crew_rec.positions) > 0:
                            for curr_pos in crew_rec.positions:
                                if new_pos.id == curr_pos.id:
                                    can_add = False
                                    break
                        if can_add:
                            model.add_skills(crew_rec, new_pos)
                    # Add a new special ability if it is not a duplicate
                    if form.ability.data > -1:
                        new_ability = model.AbilityTypes.get(form.ability.data)
                        can_add = True
                        if len(crew_rec.abilities) > 0:
                            for curr_ability in crew_rec.abilities:
                                if new_ability.id == curr_ability.id:
                                    can_add = False
                                    break
                        if can_add:
                            crew_rec.addAbilityTypes(new_ability)
                    flash('Crew record updated', 'success')
                    if form.save_and_close.data:
                        return redirect(url_for('roster.index'))
                else:
                    flash('Please correct the errors', 'danger')
            else:
                form.callsign.data = crew_rec.callsign
                form.first_name.data = crew_rec.fname
                form.last_name.data = crew_rec.lname
                form.notes.data = crew_rec.notes
                form.birthdate.data = crew_rec.bday
                form.joindate.data = crew_rec.joiningdate
                if crew_rec.rank:
                    form.rank.data = crew_rec.rank.id
                if crew_rec.gender is not None and crew_rec.gender > 0:
                    form.gender.data = crew_rec.gender

            return render_template('roster/update.html', crew_rec=crew_rec, form=form)
        except Exception:
            return "<html><body><pre>{}</pre></body></html>".format(traceback.format_exc())
    else:
        flash('You do not have permission to edit crew', 'danger')
        return redirect(url_for('roster.index'))


@bp.route('/<int:crew_id>/delete', methods=('POST',))
@login_required
def delete(crew_id):
    crew_rec = get_crew(crew_id)
    model.Crew.delete(crew_rec.id)
    return redirect(url_for('roster.index'))


@bp.route('/<int:crew_id>/detach')
@login_required
def detach(crew_id):
    try:
        parent_unit = request.args.get('parent', '')
        this_crew = model.Crew.get(crew_id)
        this_crew.unit = 0
        if parent_unit != '':
            return redirect(url_for('unit.edit', unit_id=parent_unit))
        else:
            return redirect(url_for('roster.index'))
    except sqlobject.SQLObjectNotFound:
        return abort(404)


@bp.route('/<int:crew_id>/crewtype/<int:crew_type_id>/remove')
@login_required
def remove_position(crew_id, crew_type_id):
    try:
        crew_rec = model.Crew.get(crew_id)
        crew_type_rec = model.CrewTypes.get(crew_type_id)
        result = model.PersonnelPositions.select(sqlobject.AND(model.PersonnelPositions.q.personneltype == crew_type_id,
                                                               model.PersonnelPositions.q.person == crew_id))
        success = False
        for rec in result:
            print("Removed Position", rec.id)
            model.PersonnelPositions.delete(rec.id)
            for s in crew_rec.get_skills(crew_type_id):
                print("Removed Skill", s.id)
                model.Skills.delete(s.id)
            success = True
        if success:
            flash('Removed position: {}'.format(crew_type_rec.type), 'info')
        return redirect(url_for('roster.update', crew_id=crew_id))
    except sqlobject.SQLObjectNotFound:
        return abort(404)


@bp.route('/<int:crew_id>/ability/<int:ability_type_id>/remove')
@login_required
def remove_ability(crew_id, ability_type_id):
    try:
        crew_rec = model.Crew.get(crew_id)
        ability_type_rec = model.AbilityTypes.get(ability_type_id)
        result = model.Abilities.select(sqlobject.AND(model.Abilities.q.crew == crew_rec,
                                                      model.Abilities.q.abilityType == ability_type_rec))
        success = False
        for rec in result:
            print("Removed ability", rec.id)
            model.Abilities.delete(rec.id)
            success = True
        if success:
            flash('Removed special ability: {}'.format(ability_type_rec.name), 'info')
        return redirect(url_for('roster.update', crew_id=crew_id))
    except sqlobject.SQLObjectNotFound as exc:
        print("Error getting record: ", exc)
        return abort(404)


@bp.route('random_name')
def random_name():
    gender = int(request.args.get('gender', 0))
    ancestry = int(request.args.get('ancestry', 0))
    try:
        result = generate_random_name(gender, ancestry, current_app.instance_path)
    except Exception:
        return abort(404)
    return jsonify(result)
