import os

from flask import Flask
from flask_login import LoginManager
from flask_wtf import CSRFProtect

from btechroster import model


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=''
    )

    csrf = CSRFProtect()
    csrf.init_app(app)

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # FIXME: this should not be initialized here, but where does it go?
    try:
        model.init_db(app.config.get('DATABASE'), app.config.get('DB_DRIVER'))
    except AssertionError as exc:
        print("Error loading database drivers: ", exc)

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    from . import filters
    app.register_blueprint(filters.bp)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import landing
    app.register_blueprint(landing.bp)
    app.add_url_rule('/', endpoint='index')

    from . import roster
    app.register_blueprint(roster.bp)

    from . import equipment
    app.register_blueprint(equipment.bp)

    from . import unit
    app.register_blueprint(unit.bp)

    from . import techreadout
    app.register_blueprint(techreadout.bp)

    from . import members
    app.register_blueprint(members.bp)

    from . import settings
    app.register_blueprint(settings.bp)

    from . import contracts
    app.register_blueprint(contracts.bp)

    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'

    @login_manager.user_loader
    def load_user(user_id):
        return model.Members.get(user_id)

    return app
