import csv
from pathlib import Path
import random
from .mtf_file import MtfFile
from .blk_file import BlkFile

ANCESTRY = ['Random', 'U.S.', 'English', 'Scottish', 'Irish', 'German', 'Dutch', 'Scandanavian', 'French', 'Italian', 'Hispanic', 'Portugese', 'Russian', 'Slavic', 'Polish', 'Romanian', 'Finnish', 'Albanian', 'Serbian', 'Greek', 'Turkish', 'Armenian', 'Arabic', 'African', 'Pakistani', 'Indian', 'Japanese', 'Korean', 'Chinese', 'Vietnamese', 'Indonesian', 'Polynesian', 'Filipino']

ALPHA_STRIKE_CARD_TYPES = ('BATTLEMECH',             # 0
                           'COMBAT VEHICLE',         # 1
                           'CONVENTIONAL INFANTRY',  # 2
                           'BATTLE ARMOR',           # 3
                           'AEROSPACE FIGHTER',      # 4
                           'INDUSTRIAL MECH',        # 5
                           'PROTOMECH',              # 6
                           'SUPPORT VEHICLE',        # 7
                           'CONVENTIONAL FIGHTER',   # 8
                           'SPHEROID DROPSHIP'       # 9
                           'AERODYNE DROPSHIP',      # 10
                           'SMALL CRAFT',            # 11
                           'MOBILE STRUCTURE')       # 12
ALPHA_STRIKE_CARD_UNIT_TYPE_CODES = ('BM',
                                     'CV',
                                     'CI',
                                     'BA',
                                     'AF',
                                     'IM',
                                     'PM',
                                     'SV',
                                     'CF',
                                     'DS',
                                     'DA',
                                     'SC',
                                     'MS')


def roll_skill(modifier=0, exp_modifier=0, fixed_skill_level=None):
    """Generate a dictionary with the Piloting and Gunnery skill level.

    Based on the Total Warfare tables available at the Heavy metal Pro website:

        http://www.heavymetalpro.com/DataFiles.htm#RUS_Files

    :param modifier: Adjustment to the Skill die roll, positive for better, negative for worse
    :param exp_modifier: Adjustment to the Experience die roll
    :param fixed_skill_level: Override the skill roll with a string of either "Inner Sphere", "Clan"
    :return dictionary: a dict with the Piloting and Gunnery skill levels
    """
    experience_table = {
        2: 'Green',
        3: 'Green',
        4: 'Green',
        5: 'Green',
        6: 'Regular',
        7: 'Regular',
        8: 'Regular',
        9: 'Regular',
        10: 'Veteran',
        11: 'Veteran',
        12: 'Elite',
    }
    skill_table = {
        'Green': [
            {'Piloting': 7, 'Gunnery': 6},
            {'Piloting': 6, 'Gunnery': 5},
            {'Piloting': 6, 'Gunnery': 5},
            {'Piloting': 6, 'Gunnery': 4},
            {'Piloting': 6, 'Gunnery': 4},
            {'Piloting': 5, 'Gunnery': 4},
        ],
        'Regular': [
            {'Piloting': 6, 'Gunnery': 4},
            {'Piloting': 6, 'Gunnery': 4},
            {'Piloting': 5, 'Gunnery': 4},
            {'Piloting': 5, 'Gunnery': 4},
            {'Piloting': 4, 'Gunnery': 3},
            {'Piloting': 4, 'Gunnery': 3},
        ],
        'Veteran': [
            {'Piloting': 5, 'Gunnery': 4},
            {'Piloting': 5, 'Gunnery': 4},
            {'Piloting': 4, 'Gunnery': 3},
            {'Piloting': 4, 'Gunnery': 3},
            {'Piloting': 3, 'Gunnery': 2},
            {'Piloting': 3, 'Gunnery': 2},
        ],
        'Elite': [
            {'Piloting': 4, 'Gunnery': 3},
            {'Piloting': 4, 'Gunnery': 3},
            {'Piloting': 3, 'Gunnery': 2},
            {'Piloting': 3, 'Gunnery': 2},
            {'Piloting': 2, 'Gunnery': 1},
            {'Piloting': 2, 'Gunnery': 1},
        ],
        'Inner Sphere': {'Piloting': 5, 'Gunnery': 4},
        'Clan': {'Piloting': 4, 'Gunnery': 3},
    }
    if fixed_skill_level is not None and fixed_skill_level in skill_table:
        result = skill_table[fixed_skill_level]
    else:
        roll = random.randint(1, 6) + random.randint(1, 6) + exp_modifier
        if roll < 2:
            roll = 2
        elif roll > 12:
            roll = 12
        experience = experience_table[roll]
        roll = random.randint(0, 5) + modifier
        if roll < 0:
            roll = 0
        elif roll > 5:
            roll = 5
        result = skill_table[experience][roll]
        result['Experience'] = experience
    return result


def select_weighted_ancestry(path_name):
    ancestry_codes = []
    ancestry_weights = []
    # given_name_weights = []
    with open(path_name, newline='', encoding='utf-8') as fd:
        reader = csv.reader(fd)
        for row in reader:
            ancestry_codes.append(row[0])
            ancestry_weights.append(int(row[2]))
            # given_name_weights.append(row[3:])
    result = random.choices(ancestry_codes, weights=ancestry_weights)
    return result[0]


def select_weighted_name(path_name, ancestry):
    name_list = []
    name_weight = []
    with open(path_name, newline='', encoding='utf-8') as fd:
        reader = csv.reader(fd, delimiter=",")
        for row in reader:
            if row[2] == ancestry:
                name_list.append(row[0])
                name_weight.append(int(row[1]))
    result = random.choices(name_list, weights=name_weight)
    return result[0]


def generate_random_name(gender, ancestry, source_path):
    """Generate a random name from the MegaMek source files.

    See ./docs/Rules and Technical Stuff/Random Name Generator.txt for the file format details.

    The data files used come from the ./data/names directory.

    Code based in part on megamek/client/generator/RandomNameGenerator.java

    If the gender or ancestry is 0 (i.e. no choice yet) they will be randomly generated as well.
    """
    source_dir = Path(source_path)
    result = {'first_name': 'FIRST', 'last_name': 'LAST', 'gender': gender, 'ancestry': str(ancestry)}

    surnames_fn = source_dir.joinpath('surnames.txt')
    if gender == 0:
        gender = random.randint(1, 2)
        result['gender'] = gender
    if gender == 2:
        names_fn = source_dir.joinpath('firstnames_female.txt')
    else:
        names_fn = source_dir.joinpath('firstnames_male.txt')
    # randomly determine ancestry if none was specified
    if ancestry == 0:
        result['ancestry'] = select_weighted_ancestry(source_dir.joinpath('General.csv'))
    # select first and last names based on the weighted chance
    result['last_name'] = select_weighted_name(surnames_fn, result['ancestry'])
    result['first_name'] = select_weighted_name(names_fn, result['ancestry'])
    return result
