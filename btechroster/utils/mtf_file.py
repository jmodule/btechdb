from btechroster.utils.base_import_file import BaseImportFile


class MtfImportError(Exception):
    def __init__(self, error_msg=""):
        super().__init__(error_msg)


class MtfFile(BaseImportFile):
    """Contains the data on a Mech from an MTF file.

    Based on the MegaMek java code found in megamek.common.loaders.MtfFile.java

    """
    chassisConfig = ''
    techBase = ''
    techYear = ''
    rulesLevel = ''
    source = 'Source:'

    tonnage = ''
    engine = ''
    internalType = ''
    myomerType = ''
    gyroType = ''
    cockpitType = ''
    lamType = ''
    motiveType = ''
    ejectionType = ''

    heatSinks = ''
    walkMP = ''
    jumpMP = ''
    baseChassieHeatSinks = 'base chassis heat sinks:-1'

    armorType = ''
    armorValues = []

    weaponCount = ''
    weaponData = []

    critData = []

    capabilities = ""
    deployment = ""
    overview = ""
    history = ""
    manufacturer = ""
    primaryFactory = ""
    systemManufacturers = {}
    systemModels = {}
    notes = ""
    imagePath = ""

    bv = 0

    tro_text = ''

    def __init__(self, filename=None):
        super().__init__(filename)

    def load(self, filename):
        loc = 0
        slot = 0
        with open(filename, 'r') as fd:
            self.version = fd.readline().strip()

            attr, ver_no = self.version.split(":")
            if attr != "Version" or ver_no != "1.0":
                raise MtfImportError("Invalid MTF file")

            self.name = fd.readline().strip()
            self.model = fd.readline().strip()

            # READ CRITS
            first = True
            for line in fd:
                crit = line.strip()
                if not first:
                    self.tro_text += line

                if crit == '':
                    continue
                first = False

                # if self.is_valid_location(crit):
                #     loc = self.get_location(crit)
                #     slot = 0
                #     continue

                if self.is_processed_component(crit):
                    continue

    def is_processed_component(self, line):
        try:
            crit, value = line.split(':')
            crit = crit.lower()

            if crit == 'cockpit':
                self.cockpitType = value
                return True
            if crit == 'gyro':
                self.gyroType = value
                return True
            if crit == 'lam':
                self.lamType = value
                return True
            if crit == 'motive':
                self.motiveType = value
                return True
            if crit == 'ejection':
                self.ejectionType = value
                return True
            if crit == 'mass':
                self.tonnage = value
                return True
            if crit == 'engine':
                self.engine = value
                return True
            if crit == 'structure':
                self.internalType = value
                return True
            if crit == 'myomer':
                self.myomerType = value
                return True
            if crit == 'config':
                self.chassisConfig = value
                return True
            if crit == 'techbase':
                self.techBase = value
                return True
            if crit == 'era':
                self.techYear = value
                return True
            if crit == 'source':
                self.source = value
                return True
            if crit == 'rules level':
                self.rulesLevel = value
                return True
            if crit == 'heat sinks':
                self.heatSinks = value
                return True
            if crit == 'base chassis heat sinks':
                self.baseChassieHeatSinks = value
                return True
            if crit == 'walk mp':
                self.walkMP = value
                return True
            if crit == 'jump mp':
                self.jumpMP = value
                return True
            if crit == 'armor':
                self.armorType = value
                return True
            if crit == 'overview':
                self.overview = value
                return True
            if crit == 'capabilities':
                self.capabilities = value
                return True
            if crit == 'deployment':
                self.deployment = value
                return True
            if crit == 'history':
                self.history = value
                return True
            if crit == 'manufacturer':
                self.manufacturer = value
                return True
            if crit == 'primaryfactory':
                self.primaryFactory = value
                return True
            if crit == 'systemmanufacturer':
                # self.systemManufacturers = value[1:]
                return True
            if crit == 'notes':
                self.notes = value
                return True
            if crit == 'imagefile':
                self.imagePath = value
                return True
            if crit == 'bv':
                try:
                    self.bv = int(value)
                except ValueError:
                    self.bv = 0
                return True
        except ValueError:
            pass

        return False

    def get_tech_base_value(self):
        if self.techBase == 'Inner Sphere':
            return 0
        elif self.techBase == 'Clan':
            return 1
        else:
            return 2

    def get_weight(self):
        try:
            wt = int(self.tonnage)
        except ValueError:
            wt = 0
        return wt

    def get_year(self):
        try:
            yr = int(self.techYear)
        except ValueError:
            yr = 0
        return yr

    def get_rules_level(self):
        try:
            rl = int(self.rulesLevel)
        except ValueError:
            rl = 0
        return rl
