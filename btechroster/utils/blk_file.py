from bs4 import BeautifulSoup
from btechroster.utils.base_import_file import BaseImportFile

UNIT_TYPES = ('VTOL', 'Tank', 'SupportTank', 'SupportVTOL', 'LargeSupportTank')
ARMOR_SLOTS = {
    'VTOL': {'Front': 0, 'Left': 0, 'Right': 0, 'Rear': 0, 'Rotor': 0},
    'SupportVTOL': {'Front': 0, 'Left': 0, 'Right': 0, 'Rear': 0, 'Rotor': 0},
    'Tank': {'Front': 0, 'Left': 0, 'Right': 0, 'Rear': 0, 'Turret': None},
    'SupportTank': {'Front': 0, 'Left': 0, 'Right': 0, 'Rear': 0, 'Turret': None},
    'LargeSupportTank': {'Front': 0, 'Front Right': 0, 'Front Left': 0, 'Rear Right': 0, 'Rear Left': 0, 'Rear': 0, 'Turret': None}
}
INTERNAL_TYPES = ('Standard', 'Endo-Steel', 'Endo-Steel Prototype', 'Reinforced', 'Composite')
ARMOR_TYPES = ('Standard', 'Ferro-Fiberous', 'Reactive', 'Reflective', 'Hardened', 'Light Ferro', 'Heavy Ferro', 'Stealth', 'Ferror-Fiberous Prototype', 'Commercial', 'Industrial', 'Heavy Industrial')
ENGINE_TYPES = ('Fusion', 'ICE', 'XL', 'XXL', 'LIGHT', 'COMPACT', 'Fuel Cell', 'Fission')
SUSPENSION_FACTORS = {'Hovercraft': [(10, 40),
                                     (20, 85),
                                     (30, 130),
                                     (40, 175),
                                     (50, 235)],
                      'Hydrofoil': [(10, 60),
                                    (20, 105),
                                    (30, 150),
                                    (40, 195),
                                    (50, 255),
                                    (60, 300),
                                    (70, 345),
                                    (80, 390),
                                    (90, 435),
                                    (100, 480)],
                      'Naval': [(100, 30)],
                      'Submarine': [(100, 30)],
                      'Tracked': [(100, 0)],
                      'VTOL': [(10, 50),
                               (20, 95),
                               (30, 140)],
                      'Wheeled': [(100, 20)],
                      'WiGE': [(15, 45),
                               (30, 80),
                               (45, 115),
                               (80, 140)]
                      }


class BlkFile(BaseImportFile):
    """Contains the data of a vehicle from a MegaMek building block data file."""

    block_version = ''
    unit_type = ''
    tonnage = 0
    cruise_mp = 0
    armor = {}
    equipment = {}
    tech_level = ''
    year = 0
    internal_type = 0
    armor_type = 0
    engine_type = 0
    motion_type = ''
    transporters = ''
    source = ''

    def __init__(self, filename=None):
        super().__init__(filename)

    def load(self, filename):
        with open(filename, 'r') as fd:
            blk_contents = fd.read()

        soup = BeautifulSoup(blk_contents, 'html.parser')

        self.block_version = soup.blockversion.string.strip()
        self.version = soup.version.string.strip()
        self.unit_type = soup.unittype.string.strip()
        self.name = soup.find('name').string.strip()
        self.model = soup.model.string.strip()
        try:
            self.tonnage = float(soup.tonnage.string.strip())
        except ValueError:
            pass
        try:
            self.cruise_mp = int(soup.cruisemp.string.strip())
        except ValueError:
            pass
        if self.unit_type in ARMOR_SLOTS:
            self.armor = ARMOR_SLOTS[self.unit_type].copy()
            this_armor = [a for a in soup.find('armor').string.strip().split('\n')]
            for i, k in enumerate(self.armor.keys()):
                try:
                    self.armor[k] = int(this_armor[i])
                except (ValueError, IndexError):
                    # If we can't convert the armor value, or if the vehicle doesn't have a turret (value is None)
                    # then we'll just skip the slot
                    pass
        for location in [k.lower() for k in self.armor.keys()]:
            try:
                for eqpt in soup.find(location):
                    self.equipment[location] = eqpt.string.strip()
            except TypeError:
                pass
        try:
            self.tech_level = soup.find('type').string.strip()
        except (AttributeError, TypeError, ValueError):
            pass
        try:
            self.source = soup.source.string.strip()
        except (AttributeError, TypeError, ValueError):
            pass
        try:
            self.year = int(soup.year.string.strip())
        except (AttributeError, TypeError, ValueError):
            pass
        try:
            self.internal_type = int(soup.internal_type)
        except (AttributeError, TypeError, ValueError):
            pass
        try:
            self.armor_type = int(soup.armor_type.string.strip())
        except (AttributeError, TypeError, ValueError):
            pass
        try:
            self.engine_type = int(soup.engine_type.string.strip())
        except (AttributeError, TypeError, ValueError):
            pass
        self.motion_type = soup.motion_type.string.strip()
        self.transporters = soup.transporters.string.strip()

    def weapon_summary(self):
        weapons = {}
        for loc in self.equipment:
            if self.equipment[loc].strip() != "":
                for wpn_name in self.equipment[loc].split('\n'):
                    if wpn_name in weapons:
                        weapons[wpn_name] += 1
                    else:
                        weapons[wpn_name] = 1
        weapons_out = ""
        for k in sorted(weapons):
            weapons_out += f"    {weapons[k]:2d} {k}\n"
        return weapons_out

    def engine_size(self):
        """Return an integer with the engine rating based on size, MP, and suspension"""
        suspension_factor = 0
        if self.motion_type in SUSPENSION_FACTORS:
            for wt_range in SUSPENSION_FACTORS[self.motion_type]:
                if self.tonnage <= wt_range[0]:
                    suspension_factor = wt_range[1]
        else:
            print(f"[DEBUG] Motion type not found: {self.motion_type}")
        base_rating = (self.tonnage * self.cruise_mp) - suspension_factor
        if base_rating < 10:
            base_rating = 10
        elif base_rating > 400:
            base_rating = 400
        return base_rating

    def __str__(self):
        this_engine_type = ENGINE_TYPES[self.engine_type]
        this_engine_size = self.engine_size()
        this_armor_type = ARMOR_TYPES[self.armor_type]
        cruising_speed = self.cruise_mp * 10.8
        maximum_speed = cruising_speed * 1.5
        this_armament = self.weapon_summary()
        this_armor = sum([a for a in self.armor.values() if a is not None])
        s_out = f"""{self.name} {self.model}

Mass: {self.tonnage} tons
Movement Type: {self.motion_type}
Power Plant: {this_engine_size:.0f} {this_engine_type}
Cruising Speed: {cruising_speed:.1f} kph
Maximum Speed: {maximum_speed:.1f} kph
Internal: 
Armor: {this_armor} ({this_armor_type})
Armament:
{this_armament}
"""
        return s_out

    def get_eqpt_type(self):
        return self.unit_type


if __name__ == '__main__':
    from pathlib import Path
    print("-" * 50)
    p = Path('C:\\Users\\jfriant80\\Desktop\\Bulldog Medium Tank (Standard).blk')
    vehicle = BlkFile(p)
    print(vehicle)

    print("-" * 50)
    p = Path('C:\\Users\\jfriant80\\Desktop\\Burke Defense Tank (Standard).blk')
    vehicle = BlkFile(p)
    print(vehicle)

    print("-" * 50)
    p = Path('C:\\Users\\jfriant80\\Desktop\\Jonah Submarine JN-002.blk')
    vehicle = BlkFile(p)
    print(vehicle)
