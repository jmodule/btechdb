class BaseImportFile:
    version = ''

    name = ''
    model = ''

    def __init__(self, filename=None):
        if filename is not None:
            self.load(filename)

    def load(self, unused):
        pass

    def get_full_name(self):
        return "{} {}".format(self.name, self.model)
