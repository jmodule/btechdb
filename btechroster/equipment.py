import json
import os

from flask import (
    Blueprint, flash, redirect, render_template, url_for, current_app, jsonify, request)
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from sqlobject import AND, SQLObjectNotFound
from sqlobject.sqlbuilder import LEFTJOINOn
from wtforms import StringField, SelectField, TextAreaField, DecimalField
from wtforms.validators import InputRequired, NumberRange, Optional

from btechroster import model

bp = Blueprint('equipment', __name__, url_prefix='/eqpt')


@bp.route('/', methods=('GET',))
@login_required
def index():
    if current_user.is_admin():
        s = model.Equipment.select(orderBy=(model.Equipment.q.name, model.Equipment.q.subtype))
    else:
        s = model.Equipment.select(model.Equipment.q.members == current_user,
                                   orderBy=(model.Equipment.q.name, model.Equipment.q.subtype))
    return render_template('equipment/index.html', records=s)


@bp.route('/tro/json')
def get_tro_by_type_json():
    try:
        type_id = int(request.args.get('type', None))
    except (ValueError, TypeError):
        type_id = None
    return jsonify({'entries': model.get_tro_by_type(type_id)})


class CreateEqptForm(FlaskForm):
    eqpt_type = SelectField('Type', coerce=int)
    name = StringField('Name', validators=[InputRequired()])
    subtype = StringField('Model', validators=[InputRequired()])
    weight = DecimalField('Weight', places=None, validators=[NumberRange(1, 100, "Weight must be between %(min)s and %(max)s")])
    notes = TextAreaField('Notes')
    tro = SelectField('Technical Readout', choices=[], coerce=int, validators=[Optional()])


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if current_user.is_player():
        form = CreateEqptForm()
        form.eqpt_type.choices = [(r.id, r.name) for r in model.EquipmentTypes.select(orderBy=model.EquipmentTypes.q.prefpos)]
        if form.eqpt_type.data:
            eqpt_type_id = form.eqpt_type.data
        else:
            eqpt_type_id = 1
        form.tro.choices = model.get_tro_by_type(eqpt_type_id)
        if form.validate_on_submit():
            player = model.Members.get(current_user.id)
            eqpt_type = model.EquipmentTypes.get(form.eqpt_type.data)
            reg_number = model.get_reg_number(eqpt_type)
            if form.tro.data > -1:
                this_tro = model.TechnicalReadouts.get(form.tro.data)
            else:
                this_tro = None
            new_eqpt = model.Equipment(
                eqptType=eqpt_type,
                name=form.name.data,
                subtype=form.subtype.data,
                weight=form.weight.data,
                regnumber=reg_number,
                notes=form.notes.data,
                tro=this_tro,
                members=player
            )
            flash('Equipment added successfully', 'success')
            flash('Add a pilot with the Crew selection box.', 'info')
            return redirect(url_for('equipment.edit', eqpt_id=new_eqpt.id))

        return render_template('equipment/create.html', form=form)
    else:
        flash('You do not have permission to create equipment', 'danger')
        return redirect(url_for('equipment.index'))


class EditEqptForm(FlaskForm):
    name = StringField('Name', validators=[InputRequired()])
    subtype = StringField('Model', validators=[InputRequired()])
    weight = DecimalField('Weight', places=None, validators=[NumberRange(1, 100, "Weight must be between %(min)s and %(max)s")])
    notes = TextAreaField('Notes')
    crew = SelectField('Crew', coerce=int)
    tro = SelectField('Technical Readout', coerce=int)


@bp.route('/<int:eqpt_id>/edit', methods=('GET', 'POST'))
@login_required
def edit(eqpt_id):
    if current_user.is_player():
        form = EditEqptForm()
        rec = model.Equipment.get(eqpt_id)

        result = model.Crew.select(AND(model.CrewTypes.q.equipmentType == rec.eqptType,
                                       model.CrewTypes.q.equipment == 1,
                                       model.Crew.q.members == rec.members),
                                   join=[LEFTJOINOn(None, model.PersonnelPositions,
                                                    model.Crew.q.id == model.PersonnelPositions.q.person),
                                         LEFTJOINOn(None, model.CrewTypes,
                                                    model.PersonnelPositions.q.personneltype == model.CrewTypes.q.id)],
                                   distinct=True)
        crew_list = [(c.id, c.fullName()) for c in result if len(c.equipment) == 0]
        crew_list.insert(0, (-1, 'No crew assigned...'))
        if rec.crew is not None:
            crew_list.insert(1, (rec.crew.id, rec.crew.fullName()))
        form.crew.choices = crew_list

        result = model.TechnicalReadouts.select(model.TechnicalReadouts.q.troType == rec.eqptType,
                                                orderBy=(model.TechnicalReadouts.q.name, model.TechnicalReadouts.q.subtype))
        tro_list = [(t.id, t.full_name()) for t in result]
        tro_list.insert(0, (-1, 'Link to a Technical Readout...'))
        form.tro.choices = tro_list

        if form.validate_on_submit():
            rec.name = form.name.data
            rec.subtype = form.subtype.data
            rec.weight = form.weight.data
            rec.notes = form.notes.data
            if form.crew.data != -1:
                try:
                    rec.crew = model.Crew.get(form.crew.data)
                except SQLObjectNotFound:
                    flash('Unable to add crew ID {} to the equipment'.format(form.crew.data))
            else:
                # remove the assigned crew, if any
                if rec.crew is not None:
                    rec.crew = None

            if form.tro.data != -1:
                try:
                    rec.tro = model.TechnicalReadouts.get(form.tro.data)
                except SQLObjectNotFound:
                    flash('Unable to add TRO ID {} to the equipment'.format(form.crew.data))
            else:
                if rec.tro is not None:
                    rec.tro = None

            return redirect(url_for('equipment.index'))
        else:
            form.name.data = rec.name
            form.subtype.data = rec.subtype
            form.weight.data = rec.weight
            if rec.crew is not None:
                form.crew.data = rec.crew.id
            if rec.tro is not None:
                form.tro.data = rec.tro.id

        return render_template('equipment/edit.html', form=form)
    else:
        flash('You do not have permission to edit equipment', 'danger')
        return redirect(url_for('equipment.index'))


@bp.route('/<int:eqpt_id>/delete')
@login_required
def delete(eqpt_id):
    this_eqpt = model.Equipment.get(eqpt_id)
    name = this_eqpt.name
    if this_eqpt.members.id == current_user.id:
        model.Equipment.delete(eqpt_id)
        flash('Deleted ' + name, 'success')
    else:
        flash('Unable to delete ' + name, 'warning')
    return redirect(url_for('equipment.index'))


@bp.route('/tro/json', methods=('GET',))
def tro_json():
    with open(os.path.join(current_app.instance_path, 'catalog.json')) as fd:
        catalog = json.loads(fd.read())
        meks = dict()
        for rec in catalog['entries']:
            if rec['name'] not in meks:
                meks[rec['name']] = 1
        mek_names = [(eqpt[0], eqpt[1]) for eqpt in enumerate(sorted(meks.keys()))]


@bp.route('/<int:eqpt_id>/card')
def card(eqpt_id):
    try:
        this_eqpt = model.Equipment.get(eqpt_id)
        result = render_template('equipment/card.html', this_eqpt=this_eqpt)
    except Exception as exc:
        result = "<html><body>ERROR: " + str(exc) + " in card()"
    return result
