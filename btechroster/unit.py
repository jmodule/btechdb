import glob
import os

import sqlobject
from flask import (
    Blueprint, render_template, redirect, url_for, flash,
    abort, request, current_app)
from flask_login import login_required, current_user
from flask_wtf import FlaskForm
from sqlobject import AND, IN, LIKE
from wtforms import StringField, SelectField, TextAreaField, HiddenField, SubmitField, FormField, FieldList
from wtforms.validators import InputRequired, Optional

from btechroster import model
from btechroster.utils import generate_random_name

bp = Blueprint('unit', __name__, url_prefix='/unit')


@bp.route('/', methods=('GET',))
@login_required
def index():
    if current_user.is_admin():
        s = model.Unit.select(IN(model.Unit.q.parent, (0, 1)),
                              orderBy=(model.Unit.q.parent, model.Unit.q.prefpos, model.Unit.q.id))
    else:
        s = model.Unit.select(AND(IN(model.Unit.q.parent, (0, 1)),
                                  model.Unit.q.members == current_user),
                              orderBy=(model.Unit.q.parent, model.Unit.q.prefpos, model.Unit.q.id))
    return render_template('unit/index.html', records=s)


@bp.route('/<int:unit_id>/show', methods=('GET',))
def show(unit_id):
    try:
        s = model.Unit.select(model.Unit.q.id == unit_id,
                              orderBy=(model.Unit.q.parent, model.Unit.q.prefpos, model.Unit.q.id))
        return render_template('unit/index.html', records=s)
    except sqlobject.SQLObjectNotFound:
        abort(404)


class UnitForm(FlaskForm):
    unit_id = HiddenField('Unit ID', default=-1)
    name = StringField('Unit Name', validators=[InputRequired()])
    type = SelectField('Unit Type', coerce=int)
    level = SelectField('Organization Level', coerce=int)
    limage = SelectField('Unit Image')
    subunit = SelectField('Subunit', coerce=int)
    assigned_crew = SelectField('Assigned Crew', coerce=int)
    notes = TextAreaField('Notes')
    save = SubmitField('Apply')
    save_and_close = SubmitField('Save')


def build_form(form, current_unit=None):
    if current_unit is not None:
        current_unit_id = current_unit.id
        current_unit_parent = current_unit.parent
    else:
        current_unit_id = -1
        current_unit_parent = -1

    player = model.Members.get(current_user.id)
    unit_types = [(u.id, u.name) for u in model.UnitTypes.select()]
    org_levels = [(o.id, o.name) for o in model.UnitLevel.select(orderBy=model.UnitLevel.q.prefpos)]
    image_path = os.path.join(current_app.instance_path, '..', 'btechroster', 'static', 'images', 'unittype', '*.png')
    unit_images = [(os.path.basename(n), os.path.basename(n)) for n in sorted(glob.glob(image_path))]
    detached_units = [(u.id, u.name) for u in model.Unit.select(AND(model.Unit.q.parent == 1,
                                                                    model.Unit.q.id != current_unit_id,
                                                                    model.Unit.q.id != current_unit_parent,
                                                                    model.Unit.q.members == player))]
    detached_units.insert(0, (-1, 'Add subunit...'))
    detached_crew = [(c.id, c.fullName()) for c in model.Crew.select(AND(model.Crew.q.unit == 0,
                                                                         model.Crew.q.members == player))]
    detached_crew.insert(0, (-1, 'Add crew...'))
    form.type.choices = unit_types
    form.level.choices = org_levels
    form.limage.choices = unit_images
    form.subunit.choices = detached_units
    form.assigned_crew.choices = detached_crew
    return form


@bp.route('/create', methods=('GET',))
@login_required
def create():
    if current_user.is_player():
        form = build_form(UnitForm())
        form.limage.data = 'empty.png'

        return render_template('unit/edit.html', form=form, unit_id=None, current_crew=[], subunits=[])
    else:
        flash('You do not have permission to create units', 'danger')
        return redirect(url_for('unit.index'))


@bp.route('/<int:unit_id>/edit')
@login_required
def edit(unit_id):
    if current_user.is_player():
        try:
            this_unit = model.Unit.get(unit_id)

            form = build_form(UnitForm(), this_unit)
            form.unit_id.data = this_unit.id
            form.name.data = this_unit.name
            form.level.data = this_unit.level.id
            form.type.data = this_unit.type.id
            form.notes.data = this_unit.text
            form.limage.data = this_unit.limage
        except sqlobject.SQLObjectNotFound:
            return abort(404)

        return render_template('unit/edit.html', form=form, unit_id=this_unit.id, current_crew=this_unit.crews, subunits=this_unit.subunits)
    else:
        flash('You do not have permission to create crew', 'danger')
        return redirect(url_for('unit.index'))


@bp.route('/save', methods=('POST',))
@login_required
def save():
    if current_user.is_player():
        form = build_form(UnitForm())

        if form.validate_on_submit():
            player = model.Members.get(current_user.id)
            detached_parent = model.Unit.get(1)
            this_unit_type = model.UnitTypes.get(form.type.data)
            this_unit_level = model.UnitLevel.get(form.level.data)
            if form.unit_id.data == '-1':
                this_unit = model.Unit(name=form.name.data,
                                       parent=detached_parent,
                                       type=this_unit_type,
                                       level=this_unit_level,
                                       limage=form.limage.data,
                                       text=form.notes.data,
                                       members=player)
                flash('Unit created successfully.', 'success')
            else:
                this_unit = model.Unit.get(form.unit_id.data)
                # Security Check
                if this_unit.members.id != player.id and not current_user.is_admin():
                    return abort(403)
                this_unit.name = form.name.data
                this_unit.type = this_unit_type
                this_unit.level = this_unit_level
                this_unit.limage = form.limage.data
                this_unit.text = form.notes.data
                flash('Unit updated successfully.', 'success')
            if form.subunit.data > -1:
                try:
                    subunit = model.Unit.get(form.subunit.data)
                    subunit.parent = this_unit
                except sqlobject.SQLObjectNotFound:
                    flash('Subunit {} not found'.format(form.subunit.data), 'danger')
            if form.assigned_crew.data > -1:
                try:
                    crew = model.Crew.get(form.assigned_crew.data)
                    crew.unit = this_unit
                except sqlobject.SQLObjectNotFound:
                    flash('Crew {} not found'.format(form.assigned_crew), 'danger')
            if form.save_and_close.data:
                return redirect(url_for('unit.index'))
            else:
                return redirect(url_for('unit.edit', unit_id=this_unit.id))
        else:
            flash('Please correct the errors.', 'danger')

        return render_template('unit/edit.html', form=form)
    else:
        flash('You do not have permission to create units', 'danger')
        return redirect(url_for('unit.index'))


@bp.route('/<int:unit_id>/delete')
@login_required
def delete(unit_id):
    try:
        u = model.Unit.get(unit_id)
        # Security Check
        if u.members.id == current_user.id or current_user.is_admin():
            for subunit in u.subunits:
                subunit.parent = 1
            for crew in u.crews:
                crew.unit = 0
            model.Unit.delete(u.id)
            flash('Unit deleted')
            return redirect(url_for('unit.index'))
        else:
            return abort(403)
    except sqlobject.SQLObjectNotFound:
        return abort(404)


@bp.route('/<int:unit_id>/detach')
@login_required
def detach(unit_id):
    try:
        old_parent = request.args.get('parent', '')
        this_unit = model.Unit.get(unit_id)
        this_unit.parent = model.Unit.get(1)
        if old_parent != '':
            return redirect(url_for('unit.edit', unit_id=old_parent))
        else:
            return redirect(url_for('unit.index'))
    except sqlobject.SQLObjectNotFound:
        return abort(404)


@bp.route('/<int:unit_id>/cards', methods=('GET',))
@login_required
def cards(unit_id):
    try:
        s = model.Unit.select(model.Unit.q.id == unit_id,
                              orderBy=(model.Unit.q.parent, model.Unit.q.prefpos, model.Unit.q.id))
        return render_template('unit/print.html', records=s)
    except sqlobject.SQLObjectNotFound:
        abort(404)
    # except:
    #     return traceback.print_exc(file=sys.stdout)


RANDOM_CREW = 1
LEADER_CREW = 2
INNER_SPHERE_CREW = 3
CLAN_CREW = 4
EMPTY_CREW = 5


class EquipmentWizardForm(FlaskForm):
    eqpt_type = SelectField('Type', coerce=int)
    tro = SelectField('Technical Readout', choices=[], coerce=int, validators=[Optional()])
    random_crew = SelectField('Crew', choices=[(INNER_SPHERE_CREW, 'Regular (IS)'),
                                               (CLAN_CREW, 'Veteran (Clan)'),
                                               (RANDOM_CREW, 'Random Crew'),
                                               (LEADER_CREW, 'Random Leader'),
                                               (EMPTY_CREW, 'Empty')], coerce=int)


class UnitWizardForm(FlaskForm):
    unit_name = StringField('Name', validators=[InputRequired()])
    unit_type = SelectField('Type', coerce=int, validators=[InputRequired()])
    eqpt_lines = FieldList(FormField(EquipmentWizardForm), min_entries=5)
    create = SubmitField('Create')


@bp.route('/wizard', methods=('GET', 'POST'))
@login_required
def wizard():
    if current_user.is_player():
        form = UnitWizardForm()

        form.unit_type.choices = [(u.id, u.name) for u in model.UnitTypes.select()]

        for e in form.eqpt_lines.entries:
            e.eqpt_type.choices = [(r.id, r.name) for r in model.EquipmentTypes.select(orderBy=model.EquipmentTypes.q.prefpos)]
            if e.eqpt_type.data:
                eqpt_type_id = e.eqpt_type.data
            else:
                eqpt_type_id = 1
            e.tro.choices = model.get_tro_by_type(eqpt_type_id)

        if form.validate_on_submit():
            player = model.Members.get(current_user.id)

            detached_parent = model.Unit.get(1)
            unit_type = model.UnitTypes.get(form.unit_type.data)
            this_unit = model.Unit(name=form.unit_name.data,
                                   type=unit_type,
                                   level=3,
                                   parent=detached_parent,
                                   members=player)
            for line in form.eqpt_lines.entries:
                if line.tro.data > 0:
                    try:
                        this_tro = model.TechnicalReadouts.get(line.tro.data)

                        positions = model.CrewTypes.select(model.CrewTypes.q.equipmentType == line.eqpt_type.data)
                        this_position = positions[0]

                        crew_name = generate_random_name(0, 0, current_app.instance_path)
                        join_date = model.get_current_date()
                        birth_date = model.get_random_birth_date()
                        this_crew = model.Crew(lname=crew_name['last_name'],
                                               fname=crew_name['first_name'],
                                               callsign='',
                                               notes='',
                                               gender=crew_name['gender'],
                                               joiningdate=join_date,
                                               bday=birth_date,
                                               unit=this_unit,
                                               members=player)
                        if line.random_crew.data == RANDOM_CREW:
                            model.add_skills(this_crew, this_position)
                        elif line.random_crew.data == LEADER_CREW:
                            model.add_skills(this_crew, this_position, 4)
                            try:
                                s = model.Ranks.select(LIKE(model.Ranks.q.name, '%Lieutenant%'), orderBy=model.Ranks.q.id)
                                this_crew.rank = s[0]
                            except IndexError:
                                pass
                        elif line.random_crew.data == INNER_SPHERE_CREW:
                            model.add_skills(this_crew, this_position, fixed_skill_level='Inner Sphere')
                        elif line.random_crew.data == CLAN_CREW:
                            model.add_skills(this_crew, this_position, fixed_skill_level='Clan')

                        reg_number = model.get_reg_number(this_tro.troType)

                        model.Equipment(eqptType=this_tro.troType,
                                        name=this_tro.name,
                                        subtype=this_tro.subtype,
                                        crew=this_crew,
                                        weight=this_tro.weight,
                                        regnumber=reg_number,
                                        notes='',
                                        tro=this_tro,
                                        members=player)
                    except IndexError as exc:
                        flash('Failed to add crew and equipment: ' + str(exc), 'warning')
            return redirect(url_for('unit.show', unit_id=this_unit.id))

        return render_template('unit/create.html', form=form)
    else:
        flash('You do not have permission to create units', 'danger')
        return redirect(url_for('unit.index'))
