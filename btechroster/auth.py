import datetime
import jwt

from flask import (
    Blueprint, flash, redirect, render_template, request, url_for, current_app)
from flask_login import (
    login_user, current_user, logout_user
)
from flask_wtf import FlaskForm
from werkzeug.security import check_password_hash
from werkzeug.urls import url_parse
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired, EqualTo, Email

from flask_mail import Mail, Message

from btechroster import model

bp = Blueprint('auth', __name__, url_prefix='/auth')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired()])
    password = PasswordField('Password', validators=[InputRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegisterForm(FlaskForm):
    firstname = StringField('First Name', validators=[InputRequired()])
    lastname = StringField('Last Name', validators=[InputRequired()])
    username = StringField('Username', validators=[InputRequired()])
    email = StringField('Email', validators=[InputRequired(), Email()])
    submit = SubmitField('Register')


class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[InputRequired(), Email()])
    submit = SubmitField('Request Password Reset')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[InputRequired(), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password', validators=[InputRequired()])
    submit = SubmitField('Save')


@bp.route('/register', methods=('GET', 'POST'))
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        if model.get_user(form.username.data) is not None:
            flash('User {} is already registered.'.format(form.username.data), 'warning')
        elif model.get_user_by_email(form.email.data) is not None:
            flash('Email {} is already registered.'.format(form.email.data), 'warning')
        else:
            full_name = "{} {}".format(form.firstname.data, form.lastname.data)
            user = model.Members(fname=form.firstname.data,
                                 lname=form.lastname.data,
                                 fullname=full_name,
                                 username=form.username.data,
                                 email=form.email.data)
            send_password_reset_email(user)
            flash('Registration successful, please check your email for a password reset link.', 'success')
            return redirect(url_for('auth.login'))

    return render_template('auth/register.html', form=form)


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = model.get_user(form.username.data)

        if user is not None and check_password_hash(user.get_hashed_password(), form.password.data):
            flash('Logged in successfully.', 'success')
            user.lastlogin = datetime.datetime.now()
            login_user(user, remember=form.remember_me.data)
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('index')
            return redirect(next_page)
        else:
            flash('Incorrect username or password', 'danger')

    return render_template('auth/login.html', form=form)


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


def send_password_reset_email(user):
    """Send an email to the user with a password reset URL.

    Code taken from the Flask Mega Tutorial at:

        https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-x-email-support

    """
    token = user.get_reset_password_token()
    send_email('[btech.friant.org] Reset Your Password',
               sender=current_app.config['ADMINS'][0],
               recipients=[user.email],
               text_body=render_template('auth/reset_password.txt', user=user, token=token),
               html_body=render_template('auth/reset_password.html', user=user, token=token))


@bp.route('/reset_password_request', methods=('GET', 'POST'))
def reset_password_request():
    if current_user.is_authenticated:
        return redirect((url_for('index')))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = model.Members.selectBy(email=form.email.data).getOne()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password', 'info')
        return redirect(url_for('index'))
    return render_template('auth/reset_password_request.html', form=form)


def send_email(subject, sender, recipients, text_body, html_body):
    mail = Mail(current_app)
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)


def verify_reset_password_token(token):
    try:
        members_id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])['reset_password']
    except Exception:
        return None
    return model.Members.get(members_id)


@bp.route('/reset_password/<token>', methods=('GET', 'POST'))
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        flash('Your password has been reset, please login again.', 'success')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_your_password.html', form=form, email=user.email)
