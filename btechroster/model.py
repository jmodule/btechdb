import datetime
import hashlib
import math
import random
from time import time

import jwt
import markdown
import sqlobject
from flask import current_app
from flask_login import UserMixin

from btechroster.utils import roll_skill, ALPHA_STRIKE_CARD_UNIT_TYPE_CODES


def get_tro_by_type(type_id):
    result = TechnicalReadouts.select(TechnicalReadouts.q.troType == type_id,
                                      orderBy=[TechnicalReadouts.q.name, TechnicalReadouts.q.subtype])
    choices = [(t.id, t.full_name()) for t in result]
    choices.insert(0, (-1, 'None'))
    return choices


class Abilities(sqlobject.SQLObject):
    notes = sqlobject.StringCol(length=60, default=None)
    crew = sqlobject.ForeignKey('Crew', dbName='person')
    abilityType = sqlobject.ForeignKey('AbilityTypes', dbName='ability')


class AbilityTypes(sqlobject.SQLObject):
    class sqlmeta:
        table = 'abilitytypes'

    name = sqlobject.StringCol(length=60)


class Contracts(sqlobject.SQLObject):
    start = sqlobject.DateCol()
    end = sqlobject.DateCol()
    employer = sqlobject.StringCol(length=100)
    missionType = sqlobject.StringCol(length=100, dbName='missiontype')
    target = sqlobject.StringCol(length=100)
    result = sqlobject.StringCol(length=100)
    name = sqlobject.StringCol(length=100)

    logs = sqlobject.MultipleJoin('Logentry', joinColumn='contract')

    def has_logs(self):
        return len(self.logs) > 0

    def get_logs(self, current_user_level):
        return [log_entry for log_entry in self.logs if log_entry.logtype.readpermission >= current_user_level]


class Crew(sqlobject.SQLObject):
    status = sqlobject.StringCol(length=45, default='Active')
    rank = sqlobject.ForeignKey('Ranks', dbName='rank', default=1)
    lname = sqlobject.StringCol(length=30)
    fname = sqlobject.StringCol(length=30)
    callsign = sqlobject.StringCol(length=30)
    unit = sqlobject.ForeignKey('Unit', dbName='parent', default=0)
    crewnumber = sqlobject.IntCol(default=1)
    joiningdate = sqlobject.DateCol()
    notes = sqlobject.StringCol(default='')
    bday = sqlobject.DateCol()
    notable = sqlobject.BoolCol(default=False)
    image = sqlobject.StringCol(default=None)
    members = sqlobject.ForeignKey('Members')
    gender = sqlobject.TinyIntCol(default=0)

    equipment = sqlobject.MultipleJoin('Equipment', joinColumn='crew')
    skills = sqlobject.MultipleJoin('Skills', joinColumn='person')

    abilities = sqlobject.RelatedJoin('AbilityTypes',
                                      intermediateTable='abilities',
                                      joinColumn='person',
                                      otherColumn='ability')

    positions = sqlobject.RelatedJoin('CrewTypes',
                                      intermediateTable='personnelpositions',
                                      joinColumn='person',
                                      otherColumn='personneltype')

    def fullName(self, include_rank=False):
        if include_rank and self.rank.shortName is not None:
            return "{} {} {}".format(self.rank.shortName, self.fname, self.lname)
        else:
            return "{} {}".format(self.fname, self.lname)

    def get_callsign(self):
        if self.callsign is not None and str(self.callsign).strip() != "":
            return self.callsign
        else:
            return self.fullName(True)

    def getUnitName(self):
        name = ''
        try:
            if self.unit is not None:
                name = self.unit.name
        except sqlobject.SQLObjectNotFound:
            pass
        return name

    def getEquipmentName(self):
        try:
            name = "{} {}".format(self.equipment[0].name, self.equipment[0].subtype)
        except (sqlobject.SQLObjectNotFound, IndexError):
            name = ''
        return name

    def getPlayer(self):
        player_name = ''
        try:
            if self.members is not None:
                player_name = self.members.username
        except sqlobject.SQLObjectNotFound:
            pass
        return player_name

    def getAge(self):
        try:
            s = Dates.select()
            today = s[0].currentdate
            t_diff = today - self.bday
            age = t_diff.days // 365
            return age
        except (sqlobject.SQLObjectNotFound, IndexError):
            return None

    def getASSkill(self):
        skill_value = 0
        skill_cnt = 0
        my_positions = [i.id for i in self.positions]
        for s in self.skills:
            if len(s.skillType.requirements) > 0:
                if s.skillType.requirements[0].personnelType.id in my_positions:
                    skill_value += s.value
                    skill_cnt += 1
        if skill_cnt > 0:
            result = skill_value // skill_cnt
        else:
            result = 0
        return result

    def get_skills(self, crew_type_id):
        requirements = [s.skillType.id for s in SkillRequirements.select(SkillRequirements.q.personnelType == crew_type_id)]
        return [s for s in self.skills if s.skillType.id in requirements]

    def get_point_value(self):
        total_pv = 0
        if len(self.equipment) > 0:
            if self.equipment[0].tro is not None:
                total_pv += self.equipment[0].tro.pointValue
        # The skill of the pilot will affect the total point value
        my_skill = self.getASSkill()
        if my_skill > 4:
            if total_pv > 13:
                adj = math.ceil((total_pv - 14) / 10) + 1
            else:
                adj = 1
            total_pv = total_pv - my_skill * adj
            if total_pv < 1:
                total_pv = 1
        elif my_skill < 4:
            if total_pv > 6:
                adj = math.ceil((total_pv - 7) / 5) + 1
            else:
                adj = 1
            total_pv = total_pv + my_skill * adj
        return total_pv

    def get_gender(self, short=False):
        if self.gender == 2:
            if short:
                return "F"
            else:
                return "Female"
        else:
            if short:
                return "M"
            else:
                return "Male"


def add_skills(crew_rec, new_pos, exp_modifier=0, fixed_skill_level=None):
    crew_rec.addCrewTypes(new_pos)
    result = SkillRequirements.select(SkillRequirements.q.personnelType == new_pos.id)
    skill_levels = roll_skill(exp_modifier=exp_modifier, fixed_skill_level=fixed_skill_level)
    for st in [r.skillType for r in result]:
        if st.shortname in skill_levels:
            value = skill_levels[st.shortname]
        else:
            value = 4
        Skills(crew=crew_rec, skillType=st, value=value)


class CrewTypes(sqlobject.SQLObject):
    class sqlmeta:
        table = 'crewtypes'

    type = sqlobject.StringCol(length=45)
    squad = sqlobject.IntCol()
    equipmentType = sqlobject.ForeignKey('EquipmentTypes', default=None, dbName='vehicletype')
    prefpos = sqlobject.IntCol()
    equipment = sqlobject.IntCol()

    def get_equipment_type(self):
        try:
            name = self.equipmentType.name
        except sqlobject.SQLObjectNotFound:
            name = ""
        return name


class Dates(sqlobject.SQLObject):
    startingdate = sqlobject.DateCol()
    currentdate = sqlobject.DateCol()
    endingdate = sqlobject.DateCol()

    def show_current_date(self):
        try:
            return self.currentdate.strftime("%b %d, %Y")
        except ValueError:
            return "ERROR"


def get_current_date():
    return Dates.get(1).currentdate


def get_random_birth_date():
    """Return a random date based on an age range of 18 - 35 years.

    The range value is based on Upper/Lower Age * 365.25.

    """
    today = get_current_date()
    age = random.randint(6574, 12784)
    birth_date = today - datetime.timedelta(days=age)
    return birth_date


class Equipment(sqlobject.SQLObject):
    eqptType = sqlobject.ForeignKey('EquipmentTypes', dbName='type')
    name = sqlobject.StringCol(length=45)
    subtype = sqlobject.StringCol(length=45)
    crew = sqlobject.ForeignKey('Crew', default=None, dbName='crew')
    weight = sqlobject.IntCol()
    regnumber = sqlobject.IntCol()
    notes = sqlobject.StringCol()
    tro = sqlobject.ForeignKey('TechnicalReadouts', default=None, dbName='troid')
    image = sqlobject.StringCol(length=45, default='')
    members = sqlobject.ForeignKey('Members', default=None)

    def getCrew(self):
        name = ""
        try:
            if self.crew is not None:
                name = self.crew.fullName()
        except sqlobject.SQLObjectNotFound:
            pass
        return name

    def pv(self):
        if self.crew is not None:
            return self.crew.get_point_value()
        elif self.tro is not None:
            return self.tro.pointValue
        else:
            return 0

    def get_card_code(self):
        try:
            return self.eqptType.cardCode
        except AttributeError:
            return 0

    def get_unit_card_code(self):
        try:
            return ALPHA_STRIKE_CARD_UNIT_TYPE_CODES[self.eqptType.cardCode]
        except IndexError:
            return '??'


def get_reg_number(eqpt_type):
    try:
        last_reg_number = Equipment.select(Equipment.q.eqptType==eqpt_type).max(Equipment.q.regnumber)
        reg_number = int(last_reg_number) + 1
    except (TypeError, ValueError):
        reg_number = eqpt_type.license * 10000
    return reg_number


eqpt_type_weight_scale = ('ton', 'Mton', 'kg')


class EquipmentTypes(sqlobject.SQLObject):
    class sqlmeta:
        table = 'equipmenttypes'

    name = sqlobject.StringCol(length=45)
    license = sqlobject.IntCol()
    maxWeight = sqlobject.IntCol(dbName='maxweight')
    minWeight = sqlobject.IntCol(dbName='minweight')
    weightStep = sqlobject.IntCol(dbName='weightstep')
    weightScale = sqlobject.StringCol(length=4, dbName='weightscale')
    prefpos = sqlobject.IntCol()
    used = sqlobject.BoolCol()
    crewType = sqlobject.ForeignKey('CrewTypes', default=None, dbName='requirement')
    config = sqlobject.StringCol(length=45)
    cardCode = sqlobject.IntCol()

    def get_card_code(self):
        try:
            return ALPHA_STRIKE_CARD_UNIT_TYPE_CODES[self.cardCode]
        except IndexError:
            return ""


MEMBER_TYPE = {
    1: 'Administrator',
    2: 'Game Master',
    3: 'Commander',
    4: 'Player',
    5: 'Spectator'
}


class Logentry(sqlobject.SQLObject):
    start = sqlobject.DateCol()
    end = sqlobject.DateCol()
    place = sqlobject.StringCol(length=60)
    text = sqlobject.StringCol()
    # op = sqlobject.StringCol(length=20, default='')
    # opdate = sqlobject.DateCol()
    # le = sqlobject.StringCol(length=20)
    # ledate = sqlobject.DateCol()
    topic = sqlobject.StringCol(length=60, default='')
    views = sqlobject.IntCol(default=0)
    logtype = sqlobject.ForeignKey('Logtypes', dbName='logtype')
    contract = sqlobject.ForeignKey('Contracts', dbName='contract', default=None)

    def md2html(self):
        """Convert any Markdown in the text column to HTML"""
        try:
            return markdown.markdown(str(self.text))
        except AttributeError:
            return ''


class Logtypes(sqlobject.SQLObject):
    type = sqlobject.StringCol(length=60, default='log')
    start = sqlobject.BoolCol()
    end = sqlobject.BoolCol()
    location = sqlobject.BoolCol()
    text = sqlobject.BoolCol()
    contract = sqlobject.BoolCol()
    writepermission = sqlobject.IntCol()
    readpermission = sqlobject.IntCol()
    prefpos = sqlobject.IntCol()


def log_types_allowed(user_level, reading=True):
    """Return a list of log types that the user has access to"""
    print(f'[DEBUG] reading={reading}, user_level={user_level}')
    if reading:
        result = Logtypes.select(Logtypes.q.readpermission >= user_level)
    else:
        result = Logtypes.select(Logtypes.q.writepermission >= user_level)
    return [(lt.id, lt.type) for lt in result]


class Members(sqlobject.SQLObject, UserMixin):
    username = sqlobject.StringCol(length=32)
    password = sqlobject.StringCol(length=45, default='LOCKED')
    cookie = sqlobject.StringCol(length=32, default='')
    fullname = sqlobject.StringCol(length=45, dbName='sitename')
    fname = sqlobject.StringCol(length=45)
    lname = sqlobject.StringCol(length=45)
    memberType = sqlobject.IntCol(dbName='type', length=1, default=5)
    lastlogin = sqlobject.DateTimeCol(default=datetime.datetime.now())
    postcount = sqlobject.IntCol(default=0)
    # timeoffset = sqlobject.IntCol(default=0)
    # timeformat = sqlobject.StringCol(length=20)
    # online = sqlobject.BoolCol(default=False)
    # favoredunit = sqlobject.IntCol(default=0)
    # lastlogvisited = sqlobject.IntCol(default=0)
    referer = sqlobject.StringCol(default=None)
    email = sqlobject.StringCol(length=64, default=None)

    crews = sqlobject.MultipleJoin('Crew')
    equipments = sqlobject.MultipleJoin('Equipment')
    units = sqlobject.MultipleJoin('Unit')

    def is_admin(self):
        return self.memberType <= 2

    def is_player(self):
        return self.memberType <= 4

    def get_account_type(self):
        if self.memberType in MEMBER_TYPE:
            return MEMBER_TYPE[self.memberType]
        else:
            return self.memberType

    def get_hashed_password(self):
        """Return a string with the password in the format 'Type$Salt$Hash'.

        The MercRoster database uses MD5 passwords generated by PHP that don't conform to the string format that
        hashlib expects, so this adds the correct prefix.

        """
        return 'md5$${}'.format(self.password)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256'
        ).decode('utf-8')

    def set_password(self, plaintext):
        m = hashlib.md5()
        m.update(plaintext.encode())
        self.password = m.hexdigest()


class PersonnelPositions(sqlobject.SQLObject):
    class sqlmeta:
        table = 'personnelpositions'

    personneltype = sqlobject.ForeignKey('CrewTypes', dbName='personneltype')
    person = sqlobject.ForeignKey('Crew', dbName='person')


class Ranks(sqlobject.SQLObject):
    class sqlmeta:
        idName = 'number'

    name = sqlobject.StringCol(dbName='rankname', length=45, default='')
    shortName = sqlobject.StringCol(length=10, default=None)


class Skills(sqlobject.SQLObject):
    crew = sqlobject.ForeignKey('Crew', dbName='person')
    skillType = sqlobject.ForeignKey('SkillTypes', dbName='skill')
    value = sqlobject.IntCol()


class SkillTypes(sqlobject.SQLObject):
    class sqlmeta:
        table = 'skilltypes'

    name = sqlobject.StringCol(length=60)
    shortname = sqlobject.StringCol(length=60)

    requirements = sqlobject.MultipleJoin('SkillRequirements', joinColumn='skilltype')


class SkillRequirements(sqlobject.SQLObject):
    class sqlmeta:
        table = 'skillrequirements'

    skillType = sqlobject.ForeignKey('SkillTypes', dbName='skilltype')
    personnelType = sqlobject.ForeignKey('CrewTypes', dbName='personneltype')


rules_level = ('Introductory', 'Standard', 'Advanced', 'Experimental')
technology = ('Inner Sphere', 'Clan', 'Mixed')


class TechnicalReadouts(sqlobject.SQLObject):
    """This will be using data from the Helm-Core-Fragment database.

    https://github.com/IanBellomy/helm-core-fragment
    """
    class sqlmeta:
        table = 'technicalreadouts'

    name = sqlobject.StringCol(length=60)
    subtype = sqlobject.StringCol(length=20)
    troType = sqlobject.ForeignKey('EquipmentTypes', dbName='type')
    weight = sqlobject.IntCol()
    text = sqlobject.StringCol()
    battleValue = sqlobject.IntCol(default=0)
    pointValue = sqlobject.IntCol(default=0)
    cost = sqlobject.IntCol(default=0)
    yearIntroduced = sqlobject.IntCol(default=0)
    rulesLevel = sqlobject.TinyIntCol(default=1)
    technology = sqlobject.TinyIntCol(default=0)
    unitRole = sqlobject.ForeignKey('UnitRole', default=None)
    asSize = sqlobject.TinyIntCol(default=None)
    asMove = sqlobject.StringCol(length=20, default=None)
    asArmor = sqlobject.TinyIntCol(default=0)
    asStructure = sqlobject.TinyIntCol(default=0)
    asDamageThreshold = sqlobject.TinyIntCol(default=None)
    asShort = sqlobject.FloatCol(default=None)
    asMedium = sqlobject.FloatCol(default=None)
    asLong = sqlobject.FloatCol(default=None)
    asOverheat = sqlobject.FloatCol(default=None)
    asSpecial = sqlobject.StringCol(length=100, default=None)
    mulId = sqlobject.IntCol(default=None)

    def full_name(self):
        name_out = self.name
        if self.subtype is not None:
            name_out += " " + self.subtype
        return name_out

    def getType(self):
        try:
            return self.troType.name
        except:
            return "UNKNOWN"

    def getRole(self):
        try:
            return self.unitRole.name
        except:
            return ""

    def getRulesLevel(self):
        if 0 <= self.rulesLevel < len(rules_level):
            return rules_level[self.rulesLevel]
        else:
            return "UNKNOWN"

    def getTechLevel(self):
        if 0 <= self.technology < len(technology):
            return technology[self.technology]
        else:
            return "UNKNOWN"

    def getTMM(self):
        """Return the Target Movement Modifier based on the basic movement in inches.

        I'm not sure about the formula, but I got these movement factors by plugging in numbers in the official
        Alpha Strike card generator.
        """
        try:
            pos = 0
            movement = str(self.asMove)
            for i in range(0, len(movement)):
                if not movement[i].isdigit():
                    pos = i
                    break
            mv = int(movement[:pos])
            if mv < 5:
                movement_factor = 0
            elif mv < 9:
                movement_factor = 1
            elif mv < 13:
                movement_factor = 2
            elif mv < 19:
                movement_factor = 3
            elif mv < 35:
                movement_factor = 4
            else:
                movement_factor = 5
        except ValueError:
            movement_factor = -1
        return movement_factor

    def getDamage(self, which):
        if which == 0:
            dmg = self.asShort
        elif which == 1:
            dmg = self.asMedium
        elif which == 2:
            dmg = self.asLong
        elif which == 3:
            dmg = self.asOverheat
        else:
            dmg = 0
        try:
            s = "{}".format(math.floor(dmg))
            if dmg % 1 == 0.5:
                s += "*"
        except TypeError:
            s = ""
        return s

    def getCircles(self, structure=False):
        if structure:
            cnt = self.asStructure
            css_class = "structure"
            # char_code = '&#x25EF;'  # Circle
            char_code = '&#x25A2;'  # Square with Rounded Corners
            img_fn = 'images/circle.png'
        else:
            cnt = self.asArmor
            css_class = "armor"
            char_code = '&#x25A2;'  # Square with Rounded Corners
            img_fn = 'images/box.png'
        s_out = '<span class="{}">'.format(css_class)
        for i in range(cnt):
            # s_out += '<img src="' + url_for('static', filename=img_fn) + '" style="padding: 0 2px;">'
            s_out += char_code
        s_out += "</span>"
        return s_out

    def get_special(self):
        return ", ".join(self.asSpecial.split(","))


class Unit(sqlobject.SQLObject):
    name = sqlobject.StringCol(length=45)
    type = sqlobject.ForeignKey('UnitTypes', dbName='type')
    parent = sqlobject.ForeignKey('Unit', dbName='parent', default=None)
    prefpos = sqlobject.IntCol(default=0)
    level = sqlobject.ForeignKey('UnitLevel', dbName='level', default=1)
    limage = sqlobject.StringCol(default='')
    rimage = sqlobject.StringCol(default='')
    text = sqlobject.StringCol(default='')
    members = sqlobject.ForeignKey('Members')

    subunits = sqlobject.MultipleJoin('Unit', joinColumn='parent')
    crews = sqlobject.MultipleJoin('Crew', joinColumn='parent', orderBy=(Crew.q.lname, Crew.q.fname))

    def getOwner(self):
        try:
            owner_name = self.members.username
        except:
            owner_name = ''
        return owner_name

    def get_color_code(self):
        try:
            this_color = self.type.color
        except sqlobject.SQLObjectNotFound:
            this_color = "#FFFFFF"
        return this_color

    def count_crew(self):
        cnt = 0
        if len(self.subunits) > 0:
            for u in self.subunits:
                cnt += u.count_crew()
        else:
            cnt = len(self.crews)
        return cnt

    def count_eqpt_type(self, eqpt_type):
        cnt = 0
        if len(self.subunits) > 0:
            for u in self.subunits:
                cnt += u.count_eqpt_type(eqpt_type)
        else:
            for c in self.crews:
                cnt += len([e for e in c.equipment if e.eqptType.id == eqpt_type])
        return cnt

    def get_point_value(self):
        """Calculate a total Alpha Strike Point Value for the unit."""
        total_pv = 0
        if len(self.subunits) > 0:
            for u in self.subunits:
                total_pv += u.get_point_value()
        else:
            for c in self.crews:
                total_pv += c.get_point_value()

        return total_pv

    def img_unit_level(self):
        pass

    def img_unit_type(self):
        if self.limage is not None and self.limage != '':
            return self.limage
        else:
            return 'empty.png'


class UnitLevel(sqlobject.SQLObject):
    class sqlmeta:
        table = 'unitlevel'

    name = sqlobject.StringCol(length=45)
    prefpos = sqlobject.IntCol()
    picture = sqlobject.StringCol(length=45)


class UnitRole(sqlobject.SQLObject):
    name = sqlobject.StringCol(length=45)
    notes = sqlobject.StringCol()


class UnitTypes(sqlobject.SQLObject):
    class sqlmeta:
        table = 'unittypes'

    name = sqlobject.StringCol(length=45)
    color = sqlobject.StringCol(length=45)


def init_db(db_uri, db_driver=None):
    if db_driver is not None:
        connection = sqlobject.connectionForURI(db_uri, driver=db_driver)
    else:
        connection = sqlobject.connectionForURI(db_uri)
    sqlobject.sqlhub.processConnection = connection


def get_user(username):
    try:
        user_rec = Members.selectBy(username=username).getOne()
    except sqlobject.SQLObjectNotFound:
        user_rec = None
    except sqlobject.SQLObjectIntegrityError:
        user_rec = Members.selectBy(username=username)[0]
    return user_rec


def get_user_by_email(email):
    try:
        user_rec = Members.selectBy(email=email).getOne()
    except sqlobject.SQLObjectNotFound:
        user_rec = None
    except sqlobject.SQLObjectIntegrityError:
        user_rec = Members.selectBy(email=email)[0]
    return user_rec
