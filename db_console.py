#!/usr/bin/env python3

import sys; sys.path.append('instance')
import config
from btechroster import model
model.init_db(config.DATABASE, config.DB_DRIVER)
