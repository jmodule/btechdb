#!/usr/bin/env python3
from wsgiref.handlers import CGIHandler
from btechroster import create_app

if __name__ == '__main__':
    app = create_app()
    CGIHandler().run(app)
