#!/bin/bash

source venv/bin/activate

export FLASK_APP=btechroster
export FLASK_ENV=development

flask run
