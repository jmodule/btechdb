from btechroster.utils import MtfFile


def test_mtffile():
    mtf = MtfFile('instance/Annihilator ANH-1A.mtf')
    assert mtf.name == 'Annihilator'
    assert mtf.tonnage == '100'
    assert mtf.bv == 0
