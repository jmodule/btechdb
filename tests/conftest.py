import pytest
from btechroster import create_app


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'DATABASE': '',  # FIXME: need a way to create a test database
        'DB_DRIVER': 'pymysql',
    })

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='test', password='test'):
        return self._client.post(
            '/auth/login',
            data={'username': username, 'password': password}
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def auth(client):
    return AuthActions(client)
