from btechroster import model, auth


def test_token(app):
    with app.app_context():
        this_member = model.Members.get(1)
        token = this_member.get_reset_password_token()
        test_member = auth.verify_reset_password_token(token)
        assert test_member.id == this_member.id
