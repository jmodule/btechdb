import pytest


def test_index(client, auth):
    response = client.get('/')
    assert b"Welcome" in response.data
    assert b"Log In" in response.data

    auth.login()
